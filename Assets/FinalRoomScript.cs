﻿using UnityEngine;
using System.Collections;

public class FinalRoomScript : MonoBehaviour {

	public GuardEnemy e1;
	public GuardEnemy e12;
	public GuardEnemy e13;
	public GuardEnemy e14;

	public GameLight g1;
	public GameLight g2;
	public GameLight g3;
	public GameLight g4;
	public GameLight g5;
	public GameLight g6;
	public GameLight g7;

	private AudioSource audio;

	private bool used;

	void Start()
	{
		e1.gameObject.SetActive(false);
		e12.gameObject.SetActive(false);
		e13.gameObject.SetActive(false);
		e14.gameObject.SetActive(false);
		
		g1.gameObject.SetActive(false);
		g2.gameObject.SetActive(false);
		g3.gameObject.SetActive(false);
		g4.gameObject.SetActive(false);
		g5.gameObject.SetActive(false);
		g6.gameObject.SetActive(false);
		g7.gameObject.SetActive(false);

		audio = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals("Player") & !used)
		{
			used = true;
			audio.Play();

			CheckpointSystem.checkpointIndex = -1;
			e1.gameObject.SetActive(true);
			e12.gameObject.SetActive(true);
			e13.gameObject.SetActive(true);
			e14.gameObject.SetActive(true);

			g1.gameObject.SetActive(true);
			g2.gameObject.SetActive(true);
			g3.gameObject.SetActive(true);
			g4.gameObject.SetActive(true);
			g5.gameObject.SetActive(true);
			g6.gameObject.SetActive(true);
			g7.gameObject.SetActive(true);

		}
		
	}
}
