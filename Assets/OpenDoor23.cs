﻿using UnityEngine;
using System.Collections;

public class OpenDoor23 : MonoBehaviour {

	public Door door;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			door.openDoor();
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Player")
		{
			door.closeDoor();
		}
	}
}
