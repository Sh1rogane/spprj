// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:2,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4807,x:33301,y:32588,varname:node_4807,prsc:2|diff-612-RGB,emission-932-OUT,alpha-8412-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:6670,x:31277,y:32760,varname:node_6670,prsc:2;n:type:ShaderForge.SFN_Append,id:5430,x:31470,y:32760,varname:node_5430,prsc:2|A-6670-X,B-6670-Y;n:type:ShaderForge.SFN_Multiply,id:9764,x:31665,y:32828,varname:node_9764,prsc:2|A-5430-OUT,B-9072-OUT;n:type:ShaderForge.SFN_Append,id:9072,x:31470,y:32901,varname:node_9072,prsc:2|A-5154-OUT,B-8575-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5154,x:31275,y:32923,ptovrint:False,ptlb:node_5154,ptin:_node_5154,varname:node_5154,prsc:2,glob:False,v1:1;n:type:ShaderForge.SFN_Vector1,id:8575,x:31275,y:32991,varname:node_8575,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Add,id:7455,x:31882,y:32828,varname:node_7455,prsc:2|A-9764-OUT,B-2729-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1458,x:31428,y:33190,ptovrint:False,ptlb:node_1458,ptin:_node_1458,varname:node_1458,prsc:2,glob:False,v1:5;n:type:ShaderForge.SFN_ValueProperty,id:4698,x:31428,y:33298,ptovrint:False,ptlb:node_4698,ptin:_node_4698,varname:node_4698,prsc:2,glob:False,v1:0;n:type:ShaderForge.SFN_Append,id:2846,x:31623,y:33146,varname:node_2846,prsc:2|A-1458-OUT,B-4698-OUT;n:type:ShaderForge.SFN_Time,id:7874,x:31623,y:33298,varname:node_7874,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2729,x:31815,y:33146,varname:node_2729,prsc:2|A-2846-OUT,B-7874-T;n:type:ShaderForge.SFN_OneMinus,id:7817,x:32073,y:32828,varname:node_7817,prsc:2|IN-7455-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9284,x:32255,y:32828,varname:node_9284,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7817-OUT;n:type:ShaderForge.SFN_Frac,id:9140,x:32421,y:32828,varname:node_9140,prsc:2|IN-9284-OUT;n:type:ShaderForge.SFN_Power,id:1846,x:32585,y:32828,varname:node_1846,prsc:2|VAL-9140-OUT,EXP-332-OUT;n:type:ShaderForge.SFN_Vector1,id:332,x:32421,y:33032,varname:node_332,prsc:2,v1:5;n:type:ShaderForge.SFN_Vector1,id:4406,x:32505,y:33136,varname:node_4406,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:8463,x:32703,y:32980,varname:node_8463,prsc:2|A-1846-OUT,B-4406-OUT;n:type:ShaderForge.SFN_Multiply,id:932,x:32941,y:32792,varname:node_932,prsc:2|A-8463-OUT,B-8457-RGB;n:type:ShaderForge.SFN_Color,id:8457,x:32886,y:33145,ptovrint:False,ptlb:node_8457,ptin:_node_8457,varname:node_8457,prsc:2,glob:False,c1:0,c2:0.9586205,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:612,x:32801,y:32426,ptovrint:False,ptlb:node_612,ptin:_node_612,varname:node_612,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:0.1;n:type:ShaderForge.SFN_Add,id:8412,x:33122,y:32873,varname:node_8412,prsc:2|A-612-A,B-8463-OUT;proporder:5154-1458-4698-8457-612;pass:END;sub:END;*/

Shader "Shader Forge/NewShader2" {
    Properties {
        _node_5154 ("node_5154", Float ) = 1
        _node_1458 ("node_1458", Float ) = 5
        _node_4698 ("node_4698", Float ) = 0
        _node_8457 ("node_8457", Color) = (0,0.9586205,1,1)
        _node_612 ("node_612", Color) = (1,1,1,0.1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _node_5154;
            uniform float _node_1458;
            uniform float _node_4698;
            uniform float4 _node_8457;
            uniform float4 _node_612;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _node_612.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_7874 = _Time + _TimeEditor;
                float node_8463 = (pow(frac((1.0 - ((float2(i.posWorld.r,i.posWorld.g)*float2(_node_5154,0.1))+(float2(_node_1458,_node_4698)*node_7874.g))).r),5.0)*0.5);
                float3 emissive = (node_8463*_node_8457.rgb);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,(_node_612.a+node_8463));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _node_5154;
            uniform float _node_1458;
            uniform float _node_4698;
            uniform float4 _node_8457;
            uniform float4 _node_612;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = _node_612.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                float4 node_7874 = _Time + _TimeEditor;
                float node_8463 = (pow(frac((1.0 - ((float2(i.posWorld.r,i.posWorld.g)*float2(_node_5154,0.1))+(float2(_node_1458,_node_4698)*node_7874.g))).r),5.0)*0.5);
                return fixed4(finalColor * (_node_612.a+node_8463),0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
