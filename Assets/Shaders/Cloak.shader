// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:2,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8081,x:32254,y:32576,varname:node_8081,prsc:2|diff-6335-OUT,emission-9217-OUT,alpha-6366-OUT,refract-1994-OUT;n:type:ShaderForge.SFN_Color,id:8365,x:31811,y:32474,ptovrint:False,ptlb:node_8365,ptin:_node_8365,varname:node_8365,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1691,x:31611,y:33026,ptovrint:False,ptlb:node_1691,ptin:_node_1691,varname:node_1691,prsc:2,tex:012a0fac7918f5e4faa888ab74bde203,ntxv:3,isnm:True|UVIN-3375-OUT;n:type:ShaderForge.SFN_TexCoord,id:8439,x:31185,y:33026,varname:node_8439,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:3375,x:31401,y:33026,varname:node_3375,prsc:2|A-8439-UVOUT,B-4847-OUT;n:type:ShaderForge.SFN_Vector1,id:4847,x:31214,y:33188,varname:node_4847,prsc:2,v1:4;n:type:ShaderForge.SFN_ComponentMask,id:8959,x:31679,y:33251,varname:node_8959,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-1691-RGB;n:type:ShaderForge.SFN_Multiply,id:1994,x:31901,y:33184,varname:node_1994,prsc:2|A-8959-OUT,B-5087-OUT,C-4928-OUT,D-172-OUT;n:type:ShaderForge.SFN_Vector1,id:5087,x:31710,y:33423,varname:node_5087,prsc:2,v1:0.02;n:type:ShaderForge.SFN_Slider,id:2120,x:30832,y:32869,ptovrint:False,ptlb:node_2120,ptin:_node_2120,varname:node_2120,prsc:2,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Tex2d,id:5559,x:31049,y:32537,ptovrint:False,ptlb:Dis,ptin:_Dis,varname:node_5559,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_RemapRange,id:5833,x:31258,y:32682,varname:node_5833,prsc:2,frmn:0,frmx:1,tomn:1,tomx:-1|IN-2120-OUT;n:type:ShaderForge.SFN_Add,id:3319,x:31291,y:32516,varname:node_3319,prsc:2|A-5559-R,B-5833-OUT;n:type:ShaderForge.SFN_Clamp01,id:4928,x:31495,y:32577,varname:node_4928,prsc:2|IN-3319-OUT;n:type:ShaderForge.SFN_OneMinus,id:6366,x:31787,y:32662,varname:node_6366,prsc:2|IN-4928-OUT;n:type:ShaderForge.SFN_Multiply,id:6335,x:32004,y:32629,varname:node_6335,prsc:2|A-8365-RGB,B-6366-OUT;n:type:ShaderForge.SFN_Color,id:3014,x:31520,y:32810,ptovrint:False,ptlb:node_3014,ptin:_node_3014,varname:node_3014,prsc:2,glob:False,c1:0.1172414,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:9217,x:31851,y:32860,varname:node_9217,prsc:2|A-3014-RGB,B-4928-OUT,C-172-OUT;n:type:ShaderForge.SFN_Tex2d,id:7304,x:31270,y:32256,ptovrint:False,ptlb:node_7304,ptin:_node_7304,varname:node_7304,prsc:2,tex:3a5a96df060a5cf4a9cc0c59e13486b7,ntxv:0,isnm:False;n:type:ShaderForge.SFN_RemapRange,id:9372,x:31511,y:32405,varname:node_9372,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-9266-OUT;n:type:ShaderForge.SFN_Add,id:1844,x:31604,y:32242,varname:node_1844,prsc:2|A-7304-R,B-9372-OUT;n:type:ShaderForge.SFN_Clamp01,id:172,x:31797,y:32242,varname:node_172,prsc:2|IN-1844-OUT;n:type:ShaderForge.SFN_OneMinus,id:9266,x:31291,y:32405,varname:node_9266,prsc:2|IN-2120-OUT;n:type:ShaderForge.SFN_Multiply,id:4082,x:32075,y:32820,varname:node_4082,prsc:2|A-6366-OUT,B-172-OUT;proporder:8365-1691-2120-5559-3014-7304;pass:END;sub:END;*/

Shader "Shader Forge/Cloak" {
    Properties {
        _node_8365 ("node_8365", Color) = (0.5,0.5,0.5,1)
        _node_1691 ("node_1691", 2D) = "bump" {}
        _node_2120 ("node_2120", Range(1, 0)) = 1
        _Dis ("Dis", 2D) = "white" {}
        _node_3014 ("node_3014", Color) = (0.1172414,0,1,1)
        _node_7304 ("node_7304", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float4 _node_8365;
            uniform sampler2D _node_1691; uniform float4 _node_1691_ST;
            uniform float _node_2120;
            uniform sampler2D _Dis; uniform float4 _Dis_ST;
            uniform float4 _node_3014;
            uniform sampler2D _node_7304; uniform float4 _node_7304_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 node_3375 = (i.uv0*4.0);
                float3 _node_1691_var = UnpackNormal(tex2D(_node_1691,TRANSFORM_TEX(node_3375, _node_1691)));
                float4 _Dis_var = tex2D(_Dis,TRANSFORM_TEX(i.uv0, _Dis));
                float node_4928 = saturate((_Dis_var.r+(_node_2120*-2.0+1.0)));
                float4 _node_7304_var = tex2D(_node_7304,TRANSFORM_TEX(i.uv0, _node_7304));
                float node_172 = saturate((_node_7304_var.r+((1.0 - _node_2120)*2.0+-1.0)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_node_1691_var.rgb.rg*0.02*node_4928*node_172);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float node_6366 = (1.0 - node_4928);
                float3 diffuseColor = (_node_8365.rgb*node_6366);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_node_3014.rgb*node_4928*node_172);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,node_6366),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float4 _node_8365;
            uniform sampler2D _node_1691; uniform float4 _node_1691_ST;
            uniform float _node_2120;
            uniform sampler2D _Dis; uniform float4 _Dis_ST;
            uniform float4 _node_3014;
            uniform sampler2D _node_7304; uniform float4 _node_7304_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                LIGHTING_COORDS(4,5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 node_3375 = (i.uv0*4.0);
                float3 _node_1691_var = UnpackNormal(tex2D(_node_1691,TRANSFORM_TEX(node_3375, _node_1691)));
                float4 _Dis_var = tex2D(_Dis,TRANSFORM_TEX(i.uv0, _Dis));
                float node_4928 = saturate((_Dis_var.r+(_node_2120*-2.0+1.0)));
                float4 _node_7304_var = tex2D(_node_7304,TRANSFORM_TEX(i.uv0, _node_7304));
                float node_172 = saturate((_node_7304_var.r+((1.0 - _node_2120)*2.0+-1.0)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_node_1691_var.rgb.rg*0.02*node_4928*node_172);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float node_6366 = (1.0 - node_4928);
                float3 diffuseColor = (_node_8365.rgb*node_6366);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * node_6366,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
