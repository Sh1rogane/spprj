// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:2,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:60,x:32720,y:32846,varname:node_60,prsc:2|emission-1681-OUT,alpha-4485-OUT,refract-2703-OUT;n:type:ShaderForge.SFN_Tex2d,id:253,x:32061,y:33352,ptovrint:False,ptlb:ref,ptin:_ref,varname:node_253,prsc:2,tex:012a0fac7918f5e4faa888ab74bde203,ntxv:3,isnm:True|UVIN-3174-OUT;n:type:ShaderForge.SFN_Tex2d,id:7125,x:31581,y:33046,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_7125,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_RemapRange,id:7755,x:31811,y:32837,varname:node_7755,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-6335-OUT;n:type:ShaderForge.SFN_Slider,id:6335,x:31409,y:32677,ptovrint:False,ptlb:slider,ptin:_slider,varname:node_6335,prsc:2,min:1,cur:0,max:0;n:type:ShaderForge.SFN_Add,id:4128,x:31996,y:33036,varname:node_4128,prsc:2|A-7755-OUT,B-7125-R;n:type:ShaderForge.SFN_Clamp01,id:4485,x:32188,y:33036,varname:node_4485,prsc:2|IN-4128-OUT;n:type:ShaderForge.SFN_Color,id:4315,x:32095,y:32626,ptovrint:False,ptlb:color,ptin:_color,varname:node_4315,prsc:2,glob:False,c1:0,c2:0.3793104,c3:1,c4:1;n:type:ShaderForge.SFN_OneMinus,id:3492,x:32211,y:32866,varname:node_3492,prsc:2|IN-4485-OUT;n:type:ShaderForge.SFN_Multiply,id:1681,x:32422,y:32866,varname:node_1681,prsc:2|A-4315-RGB,B-3492-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3305,x:32226,y:33352,varname:node_3305,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-253-RGB;n:type:ShaderForge.SFN_Multiply,id:2703,x:32393,y:33182,varname:node_2703,prsc:2|A-3492-OUT,B-3305-OUT,C-2239-OUT;n:type:ShaderForge.SFN_Vector1,id:2239,x:32386,y:33400,varname:node_2239,prsc:2,v1:0.02;n:type:ShaderForge.SFN_TexCoord,id:2195,x:31602,y:33383,varname:node_2195,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:3174,x:31820,y:33383,varname:node_3174,prsc:2|A-2195-UVOUT,B-7302-OUT;n:type:ShaderForge.SFN_Vector1,id:7302,x:31656,y:33588,varname:node_7302,prsc:2,v1:4;proporder:7125-6335-4315-253;pass:END;sub:END;*/

Shader "Shader Forge/Cloak3" {
    Properties {
        _Noise ("Noise", 2D) = "white" {}
        _slider ("slider", Range(1, 0)) = 0
        _color ("color", Color) = (0,0.3793104,1,1)
        _ref ("ref", 2D) = "bump" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _ref; uniform float4 _ref_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _slider;
            uniform float4 _color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_4485 = saturate(((_slider*2.0+-1.0)+_Noise_var.r));
                float node_3492 = (1.0 - node_4485);
                float2 node_3174 = (i.uv0*4.0);
                float3 _ref_var = UnpackNormal(tex2D(_ref,TRANSFORM_TEX(node_3174, _ref)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (node_3492*_ref_var.rgb.rg*0.02);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
/////// Vectors:
////// Lighting:
////// Emissive:
                float3 emissive = (_color.rgb*node_3492);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,node_4485),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
