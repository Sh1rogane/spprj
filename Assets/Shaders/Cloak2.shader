// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:2,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8621,x:33421,y:32542,varname:node_8621,prsc:2|diff-8390-RGB,alpha-2267-OUT,refract-3410-OUT;n:type:ShaderForge.SFN_TexCoord,id:3385,x:31932,y:32766,varname:node_3385,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:1339,x:31814,y:32556,varname:node_1339,prsc:2,uv:0;n:type:ShaderForge.SFN_Step,id:7159,x:32166,y:32766,varname:node_7159,prsc:2|A-3385-V,B-401-OUT;n:type:ShaderForge.SFN_OneMinus,id:5445,x:32341,y:32766,varname:node_5445,prsc:2|IN-7159-OUT;n:type:ShaderForge.SFN_Step,id:418,x:32166,y:32501,varname:node_418,prsc:2|A-2721-OUT,B-1339-V;n:type:ShaderForge.SFN_OneMinus,id:2219,x:32347,y:32501,varname:node_2219,prsc:2|IN-418-OUT;n:type:ShaderForge.SFN_Multiply,id:6042,x:32526,y:32631,varname:node_6042,prsc:2|A-2219-OUT,B-5445-OUT;n:type:ShaderForge.SFN_Slider,id:2433,x:31469,y:32755,ptovrint:False,ptlb:node_2433,ptin:_node_2433,varname:node_2433,prsc:2,min:0.6,cur:0,max:0;n:type:ShaderForge.SFN_OneMinus,id:2721,x:31976,y:32403,varname:node_2721,prsc:2|IN-2433-OUT;n:type:ShaderForge.SFN_Add,id:401,x:31932,y:32940,varname:node_401,prsc:2|A-2433-OUT,B-8833-OUT;n:type:ShaderForge.SFN_Vector1,id:8833,x:31756,y:33009,varname:node_8833,prsc:2,v1:0;n:type:ShaderForge.SFN_Color,id:8390,x:32930,y:32335,ptovrint:False,ptlb:node_8390,ptin:_node_8390,varname:node_8390,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5027,x:32543,y:32952,ptovrint:False,ptlb:node_5027,ptin:_node_5027,varname:node_5027,prsc:2,tex:012a0fac7918f5e4faa888ab74bde203,ntxv:3,isnm:True|UVIN-7877-OUT;n:type:ShaderForge.SFN_TexCoord,id:2802,x:32133,y:32952,varname:node_2802,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:7877,x:32341,y:32952,varname:node_7877,prsc:2|A-2802-UVOUT,B-1820-OUT;n:type:ShaderForge.SFN_Vector1,id:1820,x:32150,y:33164,varname:node_1820,prsc:2,v1:3;n:type:ShaderForge.SFN_OneMinus,id:2267,x:32756,y:32631,varname:node_2267,prsc:2|IN-6042-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5704,x:32739,y:32998,varname:node_5704,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-5027-RGB;n:type:ShaderForge.SFN_Multiply,id:4520,x:32956,y:32998,varname:node_4520,prsc:2|A-5704-OUT,B-6042-OUT;n:type:ShaderForge.SFN_Multiply,id:3410,x:33170,y:32998,varname:node_3410,prsc:2|A-4520-OUT,B-7255-OUT;n:type:ShaderForge.SFN_Vector1,id:7255,x:32956,y:33173,varname:node_7255,prsc:2,v1:0.01;n:type:ShaderForge.SFN_Color,id:5161,x:32924,y:32494,ptovrint:False,ptlb:node_5161,ptin:_node_5161,varname:node_5161,prsc:2,glob:False,c1:0,c2:0.7931032,c3:1,c4:1;n:type:ShaderForge.SFN_Step,id:5687,x:32243,y:32069,varname:node_5687,prsc:2|A-1241-OUT,B-1339-V;n:type:ShaderForge.SFN_Add,id:1241,x:32024,y:32069,varname:node_1241,prsc:2|A-2721-OUT,B-3477-OUT;n:type:ShaderForge.SFN_Vector1,id:3477,x:31834,y:32088,varname:node_3477,prsc:2,v1:0.01;n:type:ShaderForge.SFN_Step,id:4776,x:32243,y:32224,varname:node_4776,prsc:2|A-3385-V,B-2271-OUT;n:type:ShaderForge.SFN_Subtract,id:2271,x:32014,y:32224,varname:node_2271,prsc:2|A-401-OUT,B-1165-OUT;n:type:ShaderForge.SFN_Vector1,id:1165,x:31769,y:32244,varname:node_1165,prsc:2,v1:0.01;n:type:ShaderForge.SFN_OneMinus,id:7387,x:32463,y:32068,varname:node_7387,prsc:2|IN-5687-OUT;n:type:ShaderForge.SFN_OneMinus,id:8324,x:32463,y:32224,varname:node_8324,prsc:2|IN-4776-OUT;n:type:ShaderForge.SFN_Multiply,id:5084,x:32651,y:32140,varname:node_5084,prsc:2|A-7387-OUT,B-8324-OUT;n:type:ShaderForge.SFN_Multiply,id:8556,x:32983,y:32768,varname:node_8556,prsc:2|A-5084-OUT,B-5161-RGB,C-8576-RGB;n:type:ShaderForge.SFN_Tex2d,id:8576,x:32711,y:32412,ptovrint:False,ptlb:node_8576,ptin:_node_8576,varname:node_8576,prsc:2,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:3,isnm:False;proporder:2433-8390-5027-5161-8576;pass:END;sub:END;*/

Shader "Shader Forge/Cloak2" {
    Properties {
        _node_2433 ("node_2433", Range(0.6, 0)) = 0
        _node_8390 ("node_8390", Color) = (0.5,0.5,0.5,1)
        _node_5027 ("node_5027", 2D) = "bump" {}
        _node_5161 ("node_5161", Color) = (0,0.7931032,1,1)
        _node_8576 ("node_8576", 2D) = "bump" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float _node_2433;
            uniform float4 _node_8390;
            uniform sampler2D _node_5027; uniform float4 _node_5027_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 node_7877 = (i.uv0*3.0);
                float3 _node_5027_var = UnpackNormal(tex2D(_node_5027,TRANSFORM_TEX(node_7877, _node_5027)));
                float node_2721 = (1.0 - _node_2433);
                float node_401 = (_node_2433+0.0);
                float node_6042 = ((1.0 - step(node_2721,i.uv0.g))*(1.0 - step(i.uv0.g,node_401)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_node_5027_var.rgb.rg*node_6042)*0.01);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _node_8390.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,(1.0 - node_6042)),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform float _node_2433;
            uniform float4 _node_8390;
            uniform sampler2D _node_5027; uniform float4 _node_5027_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                LIGHTING_COORDS(4,5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 node_7877 = (i.uv0*3.0);
                float3 _node_5027_var = UnpackNormal(tex2D(_node_5027,TRANSFORM_TEX(node_7877, _node_5027)));
                float node_2721 = (1.0 - _node_2433);
                float node_401 = (_node_2433+0.0);
                float node_6042 = ((1.0 - step(node_2721,i.uv0.g))*(1.0 - step(i.uv0.g,node_401)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_node_5027_var.rgb.rg*node_6042)*0.01);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = _node_8390.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * (1.0 - node_6042),0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
