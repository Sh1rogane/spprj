// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,dith:2,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1832,x:33602,y:32658,varname:node_1832,prsc:2|diff-640-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:8263,x:32488,y:32599,varname:node_8263,prsc:2;n:type:ShaderForge.SFN_ViewPosition,id:2993,x:32488,y:32849,varname:node_2993,prsc:2;n:type:ShaderForge.SFN_Distance,id:1437,x:32680,y:32726,varname:node_1437,prsc:2|A-8263-XYZ,B-2993-XYZ;n:type:ShaderForge.SFN_Divide,id:1091,x:32850,y:32726,varname:node_1091,prsc:2|A-1437-OUT,B-4945-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4945,x:32698,y:32922,ptovrint:False,ptlb:Dist,ptin:_Dist,varname:node_4945,prsc:2,glob:True,v1:1;n:type:ShaderForge.SFN_Power,id:9598,x:33019,y:32725,varname:node_9598,prsc:2|VAL-1091-OUT,EXP-7087-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7087,x:32870,y:32922,ptovrint:False,ptlb:node_7087,ptin:_node_7087,varname:node_7087,prsc:2,glob:False,v1:500;n:type:ShaderForge.SFN_Clamp01,id:7019,x:33179,y:32725,varname:node_7019,prsc:2|IN-9598-OUT;n:type:ShaderForge.SFN_Color,id:503,x:33514,y:32480,ptovrint:False,ptlb:Away,ptin:_Away,varname:node_503,prsc:2,glob:False,c1:0.7867647,c2:0.7867647,c3:0.7867647,c4:1;n:type:ShaderForge.SFN_Color,id:6900,x:33280,y:32480,ptovrint:False,ptlb:Near,ptin:_Near,varname:node_6900,prsc:2,glob:False,c1:1,c2:0.4044118,c3:0.4044118,c4:1;n:type:ShaderForge.SFN_Lerp,id:640,x:33404,y:32725,varname:node_640,prsc:2|A-6900-RGB,B-503-RGB,T-7019-OUT;proporder:7087-503-6900;pass:END;sub:END;*/

Shader "Shader Forge/Test3" {
    Properties {
        _node_7087 ("node_7087", Float ) = 500
        _Away ("Away", Color) = (0.7867647,0.7867647,0.7867647,1)
        _Near ("Near", Color) = (1,0.4044118,0.4044118,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _Dist;
            uniform float _node_7087;
            uniform float4 _Away;
            uniform float4 _Near;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = lerp(_Near.rgb,_Away.rgb,saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/_Dist),_node_7087)));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _Dist;
            uniform float _node_7087;
            uniform float4 _Away;
            uniform float4 _Near;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = lerp(_Near.rgb,_Away.rgb,saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/_Dist),_node_7087)));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
