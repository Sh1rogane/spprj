﻿using UnityEngine;
using System.Collections;

public class SpawnEnemyDL1 : MonoBehaviour {

	public GuardEnemy enemy;
	public Door door;

	// Use this for initialization
	void Start () {
		enemy.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals("Player"))
		{
			enemy.gameObject.SetActive(true);
			door.openDoor();
		}
	}
}
