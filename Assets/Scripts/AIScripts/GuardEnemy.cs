using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GuardEnemy : MonoBehaviour 
{
	//
	//States
	public enum States {Idle, Patrolling, Investigate, Chasing, Search, SearchInvestigate, LightInvestigate, Attacking};
	private States currentState = States.Idle;

	//Component stuff
	private Animator animator;
	private NavMeshAgent navAgent;
	private AudioSource stepAudio;
	private AudioSource mouthSound;
	public AudioClip huhSound;
	public AudioClip detectedSound;
	public AudioClip sawSomethingSound;
	public float mouthVolume = 0.5f;
	//NavmeshAgent
	private float walkSpeed = 2f;
	private float chaseSpeed = 4f;

	//Start stuff
	private Vector3 startPos;
	private Quaternion startRot;

	private Transform head;

	//Field of view stuff
	private float fieldOfView = 140f;
	private float viewDistance = 20f;

	private Player player;

	//Detected meter stuff
	//0 - Nothing
	//100 - Know
	private float detectedMeter = 0;
	private bool detectedIncrease;
	private float detectedITimer = 0;

	//Knowledge stuff
	private Vector3 lastSeenPos = Vector3.zero;
	private Vector3 investigatePos = Vector3.zero;

	//Investigate
	private float stayTimer = 0f;
	private float alertClamp = 0f;

	//Patrol stuff
	public List<PatrolNode> patrolNodes = new List<PatrolNode>();
	private int currentNodeIndex = 0;
	//private float pauseTime;

	//Chase
	private float chaseTimer = 0f;
	private float chaseTime = 3f;
	private Vector3 chasePosition = Vector3.zero;

	//Search
	private List<SearchNode> searchNodes = new List<SearchNode>(); 

	//Light
	private LightSwitch lightSwitch;
	private bool hasSwitched;

	//Guard communication
	private List<GuardEnemy> g = new List<GuardEnemy>(); 
	public float range = 15f;
	private bool hasContacted = false;
	private bool hasOrders = false;

	//Player Gadgets
	private MotionSensor ms;

	//Stuck checking
	private float stuckTimer;
	//private bool stuck;


	//Flashlight
	private Flashlight flashlight;


	// Use this for initialization
	void Start () 
	{
		GameObject[] gos =  GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject go in gos)
		{
			g.Add(go.GetComponent<GuardEnemy>());
		}
		g.Remove(this);

		animator = GetComponent<Animator>();
		navAgent = GetComponent<NavMeshAgent>();
		stepAudio = transform.Find("Basic_Guard").GetComponent<AudioSource>();
		mouthSound = GetComponent<AudioSource>();
		head = transform.Find("Basic_Guard/Root/Spine_01/Neck/Headjoint/Headtip");

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		flashlight = GetComponentInChildren<Flashlight>();

		startPos = transform.position;
		startRot = transform.rotation;

		if(patrolNodes.Count > 0)
		{
			currentState = States.Patrolling;
			navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
		}	
		mouthSound.clip = huhSound;
	}
	void FixedUpdate()
	{
		if(currentState == States.Investigate || currentState == States.LightInvestigate || currentState == States.Search || currentState == States.SearchInvestigate)
		{
			checkStuck();
		}
		if(ms)
		{
			if(ms.isMoving)
			{
				gadgetTrace();
			}
		}

		seeing();
		if(currentState == States.Idle)
		{
			idle();
		}
		else if(currentState == States.Patrolling)
		{
			patroll();
		}
		else if(currentState == States.Investigate)
		{
			investigate();
		}
		else if(currentState == States.Chasing)
		{
			chasing();
		}
		else if(currentState == States.Search)
		{
			search();
		}
		else if(currentState == States.SearchInvestigate)
		{
			searchInvestigate();
		}
		else if(currentState == States.LightInvestigate)
		{
			lightInvestigate();
		}
		else if(currentState == States.Attacking)
		{
			attacking();
		}
		if(checkIfNeedLight())
		{
			animator.SetBool("useFlashlight", true);
			flashlight.turnOn();
		}
		else
		{
			flashlight.turnOff();
			animator.SetBool("useFlashlight", false);
		}
	}
	private void checkStuck()
	{
		float checkVelocity = navAgent.desiredVelocity.magnitude - navAgent.velocity.magnitude;
		if (!navAgentDone())
		{
			if (checkVelocity > 0.1f) 
			{
				stuckTimer += Time.fixedDeltaTime;
				if(stuckTimer > 0.5f)
				{
					navAgent.SetDestination(transform.position);
					stuckTimer = 0;
				}
			}
			else
			{
				stuckTimer -= Time.fixedDeltaTime;
				if(stuckTimer < 0f)
				{
					stuckTimer = 0f;
				}
			}
		}
	}
	private void attacking()
	{
		if(navAgentDone())
		{
			Quaternion lookRotation = Quaternion.LookRotation(getEyePosition() - player.getEyePosition());
			transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.fixedDeltaTime * 90);
		}
		animator.SetTrigger("attack");
		if(player.alive)
		{
			//mouthSound.PlayOneShot(huhSound, 1f);
		}
		player.kill();

	}
	private void contactOthers(Vector3 pos,States s)
	{
		/*if(!hasOrders)
		{
			foreach(GuardEnemy ge in g)
			{
				float dist = Vector3.Distance(this.transform.position, ge.transform.position);
				Debug.Log (dist);
				if(dist <= range)
				{
					ge.recieveOrder(pos, s);
				}
			}
		}*/


	}
	public void recieveOrder(Vector3 p, States s)
	{
		hasOrders = true;

		lastSeenPos = p;

		currentState = s;
		if(s == States.Investigate)
		{
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		} else if(s == States.Chasing)
		{
			navAgent.SetDestination(lastSeenPos);
			chasePosition = lastSeenPos;
		}

	}
	private bool checkIfNeedLight()
	{
		Collider[] cols = Physics.OverlapSphere(getEyePosition(), 7f, 1 << 16);

		foreach(Collider col in cols)
		{
			Vector3 targetDir = col.transform.position - getEyePosition();
			RaycastHit hit;
			
			if (Physics.Raycast(getEyePosition(), targetDir, out hit, 7f)) 
			{
				//Debug.DrawLine(transform.position, hit.point, Color.red);
				if(hit.collider.gameObject.tag.Equals("Light"))
				{
					GameLight gl = hit.collider.GetComponent<GameLight>();
					if(!gl.on)
						return true;
				}
			}
		}
		if(cols.Length == 0)
		{
			return true;
		}
		return false;
	}
	private void gadgetTrace()
	{
		//objekt i synfält + raycast träff


		if(positionInsideFOV(ms.transform.position))
		{
			Vector3 targetDir = ms.transform.position - getEyePosition();
			RaycastHit hit;
			
			if (Physics.Raycast(getEyePosition(), targetDir, out hit, viewDistance)) 
			{
				if(hit.collider.gameObject.tag.Equals("Gadget"))
				{
					//Debug.DrawLine(getEyePosition(), hit.point, Color.red, 0.1f);
					calculatePossibleOrigin(hit.point);
					mouthSound.PlayOneShot(sawSomethingSound, mouthVolume);
				}
				//Debug.DrawLine(getEyePosition(), hit.point, Color.green, 0.1f);
			}
		}	
	}
	private void calculatePossibleOrigin(Vector3 point)
	{
		//hämta velocity - y-velocity -> räkna ut vart 
		Vector3 vel = ms.rbody.velocity;
		vel.y = 0;
		vel *= -1;
		
		vel *= 10 * Time.fixedDeltaTime;
		Vector3 origin = point + vel;

		ms = null;

		lastSeenPos = origin;
		currentState = States.Investigate;
		navAgent.SetDestination(lastSeenPos);
		investigatePos = lastSeenPos;
	

	}
	public void investigateLight (LightSwitch ls)
	{
		CancelInvoke();
		investigatePos = ls.transform.position;
		currentState = States.LightInvestigate;
		navAgent.SetDestination(investigatePos);
		hasSwitched = false;
		lightSwitch = ls;

		if(!hasContacted)
		{
			contactOthers(investigatePos, States.Investigate);
			hasContacted = true;
		}

	}
	private void lightInvestigate()
	{
		alertClamp = 30f;
		//Walk to last seen "suspected" position;
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
		//Debug.Log(navAgent.remainingDistance);
		if(navAgentDone() && !hasSwitched)
		{

			hasSwitched = true;
			animator.SetTrigger("use");

		}

		if(animator.GetCurrentAnimatorStateInfo(0).IsName("Use") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
		{
			investigatePos = Vector3.zero;
			stayTimer = 0f;
			alertClamp = 0f;
			if(patrolNodes.Count > 0)
			{
				currentState = States.Patrolling;
				navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
			}
			else
			{
				currentState = States.Idle;
				navAgent.SetDestination(startPos);
			}
			hasSwitched = false;
			hasContacted = false;
			hasOrders = false;
		}
	}
	public void EnemyUse()
	{
		lightSwitch.enemyUse();
	}
	public void cameraChase()
	{
		chasePosition = player.transform.position;
		lastSeenPos = chasePosition;
		navAgent.SetDestination(chasePosition);
		currentState = States.Chasing;
		detectedMeter = 100f;
	}
	private void gatherSearchNodes(Vector3 center, float radius)
	{
		searchNodes.Clear();

		GameObject[] gos = GameObject.FindGameObjectsWithTag("SearchNode");

		float close = Mathf.Infinity;
		GameObject closeGo = null;

		foreach(GameObject go in gos)
		{
			float dist = Vector3.Distance(center, go.transform.position);
			if(dist < close)
			{
				//searchNodes.Add(go.GetComponent<SearchNode>());
				closeGo = go;
				close = dist;
			}
		}

		searchNodes.AddRange(closeGo.transform.parent.GetComponentsInChildren<SearchNode>());
	}
	private void searchInvestigate()
	{
		alertClamp = 30f; 
		//Walk to last seen "suspected" position;
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
		
		if(navAgentDone())
		{
			stayTimer += Time.fixedDeltaTime;
			if(stayTimer > 3f)
			{
				investigatePos = Vector3.zero;
				stayTimer = 0f;
				currentState = States.Search;
				navAgent.SetDestination(searchNodes[0].transform.position);
			}
		}
	}
	private void search()
	{
		alertClamp = 30f;
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.SearchInvestigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}

		if(navAgentDone())
		{
			searchNodes.RemoveAt(0);
		}
		if(searchNodes.Count > 0)
		{
			navAgent.SetDestination(searchNodes[0].transform.position);
		}
		else
		{
			alertClamp = 0f;
			if(patrolNodes.Count > 0)
			{
				currentState = States.Patrolling;
				navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
				chasePosition = Vector3.zero;
				investigatePos = Vector3.zero;
			}
			else
			{
				currentState = States.Idle;
				navAgent.SetDestination(startPos);
				chasePosition = Vector3.zero;
				investigatePos = Vector3.zero;
			}
		}
		
	}
	private void chasing()
	{
		navAgent.speed = chaseSpeed;
		if(!hasContacted)
		{
			contactOthers(chasePosition,States.Chasing);
			hasContacted = true;
		}

		if(navAgentDone())
		{
			chaseTimer += Time.fixedDeltaTime;
			if(chaseTimer >= chaseTime)
			{
				chaseTimer = 0f;
				detectedMeter = 30;
				navAgent.speed = walkSpeed;
				gatherSearchNodes(transform.position, 20f);
				navAgent.SetDestination(searchNodes[0].transform.position);
				currentState = States.Search;
				hasOrders = false;
			}

		}
		float dist = Vector3.Distance(getEyePosition(), player.getEyePosition());
		if(dist < 1f)
		{
			navAgent.Stop ();
			currentState = States.Attacking;
		}

	}
	private void investigate()
	{

		if(!hasContacted)
		{
			contactOthers(investigatePos,States.Investigate);
			hasContacted = true;
		}
		alertClamp = 30f;
		//Walk to last seen "suspected" position;
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}

		if(navAgentDone())
		{
			stayTimer += Time.fixedDeltaTime;
			if(stayTimer > 3f)
			{
				investigatePos = Vector3.zero;
				stayTimer = 0f;
				alertClamp = 0f;
				if(patrolNodes.Count > 0)
				{
					currentState = States.Patrolling;
					navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
				}
				else
				{
					currentState = States.Idle;
					navAgent.SetDestination(startPos);
				}
				hasContacted = false;
				hasOrders = false;
			}
		}


	}
	private void patroll()
	{
		//Enemy thinks he sees something
		if(detectedMeter > 50 && detectedMeter < 100)
		{
			mouthSound.PlayOneShot(sawSomethingSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		//Enemy sees the player
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
	}
	private void idle()
	{

		if(navAgentDone())
		{
			transform.rotation = Quaternion.RotateTowards(transform.rotation, startRot, Time.fixedDeltaTime * 90);
		}
		//Enemy thinks he sees something
		if(detectedMeter > 50 && detectedMeter < 100)
		{
			mouthSound.PlayOneShot(sawSomethingSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		//Enemy sees the player
		else if(detectedMeter >= 100)
		{
			mouthSound.PlayOneShot(detectedSound, mouthVolume);
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
	}
	// Update is called once per frame
	void Update () 
	{
		//navAgent.SetDestination(target.position);
		animator.SetFloat("speed", navAgent.desiredVelocity.magnitude);
	}


	void OnTriggerEnter(Collider col)
	{
		if(currentState == States.Patrolling)
		{
			if(col.gameObject.tag.Equals("Node"))
			{
				PatrolNode n = col.gameObject.GetComponent<PatrolNode>();
				if(patrolNodes[currentNodeIndex] == n)
				{
					currentNodeIndex++;
					if(currentNodeIndex >= patrolNodes.Count)
					{
						currentNodeIndex = 0;
					}
					Invoke("changePatrolNode", n.pauseTime); 
				}
			}
		}
		if(col.gameObject.tag.Equals("Player"))
		{
			//
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}

	}
	void changePatrolNode()
	{
		navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
	}
	bool navAgentDone()
	{
		if (!navAgent.pathPending)
		{
			if (navAgent.remainingDistance <= navAgent.stoppingDistance)
			{
				if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude == 0f)
				{
					return true;
				}
			}
		}
		return false;
	}
	void seeing()
	{
		//First check if player is inside the FOV
		if(positionInsideFOV(player.transform.position))
		{
			//If player is inside FOV do a raycast to the player with a small random factor
			if(raycastToPlayerRandom())
			{
				if(currentState == States.Chasing)
				{
					chasePosition = player.transform.position;
					navAgent.SetDestination(chasePosition);
				}

				//If player is hit, calculate the visibility
				float dm = checkPlayerVisibility();
				//If visibility is higher than 0 (the enemy can see atleast something)
				if(dm > 0)
				{
					//Add the calulated value to detected meter
					detectedITimer = 0f;
					detectedIncrease = true;
					detectedMeter += dm;
				}
			}
		}
		//This is used to indicate when it is time to lower detected meter (if enemy can't see anything in 1 second)
		if(detectedIncrease)
		{
			detectedITimer += Time.fixedDeltaTime;
			if(detectedITimer > 1f)
			{
				detectedIncrease = false;
			}
		}
		else
		{

			detectedMeter -= 1;
		}
		//Clamp detected meter
		detectedMeter = Mathf.Clamp(detectedMeter, alertClamp, 200);
		//print (detectedMeter);

	}
	float checkPlayerVisibility()
	{
		float dist = Vector3.Distance(getEyePosition(), player.getRaycastPosition());
		float playerVisibility = player.getVisibility();
		float relativeDist = (viewDistance - dist) / 5; 
		if(relativeDist < 1)
		{
			relativeDist = 1;
		}
		//print (dist);
		float dm = relativeDist * (playerVisibility / 2);


		//dist = 20 , vis = 5;
		//relDist 2 * 5 10 1/5 sekund

		if(dist <= 2f && playerVisibility < 1)
		{
			return 3;
		}

		//detectedMeter += dm;
		return dm;
	}
	bool raycastToPlayer()
	{
		Vector3 targetDir = (player.getRaycastPosition()) - getEyePosition();
		RaycastHit hit;
		
		if (Physics.Raycast(getEyePosition(), targetDir, out hit, viewDistance)) 
		{
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				//Debug.DrawLine(getEyePosition(), hit.point, Color.red, 0.1f);
				return true;
			}
			//Debug.DrawLine(getEyePosition(), hit.point, Color.green, 0.1f);
		}
		return false;
	}
	bool raycastToPlayerRandom()
	{
		Vector3 targetDir = (getRandomPlayerPosition()) - getEyePosition();
		RaycastHit hit;
		
		if (Physics.Raycast(getEyePosition(), targetDir, out hit, 15f)) 
		{
			
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				//Debug.DrawLine(getEyePosition(), hit.point, Color.red, 0.1f);
				return true;
			}
			//Debug.DrawLine(getEyePosition(), hit.point, Color.green, 0.1f);
		}
		return false;
	}
	bool positionInsideFOV(Vector3 position)
	{		
		Vector3 targetDir = position - getEyePosition();
		float a = Vector3.Angle(targetDir, head.forward);
		//float dist = Vector3.Distance(getEyePosition(), player.getRaycastPosition());
		//print (a);
		if((a < fieldOfView / 2))
		{
			return true;
		}
		return false;
	}
	private Vector3 getRandomPlayerPosition()
	{
		Vector3 pos = player.getRaycastPosition();
		pos += new Vector3(Random.Range(-0.5f,0.5f), Random.Range(-0.5f,0.5f), Random.Range(-0.5f,0.5f));
		return pos;
	}
	private Vector3 getEyePosition()
	{
		Vector3 pos = head.position;
		pos.y -= 0.1f;

		return pos;
	}
	public void heardSoundThroughPath(Vector3 soundPosition)
	{
		if(currentState == States.Idle || currentState == States.Patrolling)
		{
			mouthSound.PlayOneShot(huhSound, mouthVolume);
		}
		if(currentState == States.Idle || currentState == States.Patrolling || currentState == States.Investigate || currentState == States.LightInvestigate)
		{
			CancelInvoke();
			detectedMeter = 50;
			lastSeenPos = soundPosition;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(currentState == States.Chasing)
		{
			chasePosition = player.transform.position;
			navAgent.SetDestination(chasePosition);
		}
		else if(currentState == States.Search || currentState == States.SearchInvestigate)
		{
			lastSeenPos = player.transform.position;
			currentState = States.SearchInvestigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}


	}
	public void StepSound()
	{
		stepAudio.Play ();
	}
	void OnAnimatorIK(int layerIndex)
	{
		if(detectedMeter > 50f)
		{
			animator.SetLookAtWeight(1f);
			animator.SetLookAtPosition(Player.instance.getEyePosition());
		}

	}
	public Vector3 getRaycastPosition()
	{
		Vector3 pos = transform.position;
		pos.y += 0.9f;
		return pos;
	}
	void OnDrawGizmosSelected()
	{
		drawView ();
		drawPatrolPath();
	}
	private void drawView()
	{
		Gizmos.color = Color.magenta;
		if(head != null)
		{
			Vector3 start = head.position;
			start.y -= 0.1f;
			
			float halfAngle = fieldOfView / 2f;
			
			float ca = -halfAngle;
			float l = fieldOfView / 10f;
			float la = fieldOfView / l;
			for(int i = 0; i < l; i++)
			{
				Quaternion q1 = Quaternion.Euler (head.rotation.eulerAngles.x, head.rotation.eulerAngles.y + ca, head.rotation.eulerAngles.z);
				Vector3 forward = q1 * Vector3.forward;
				forward.Normalize ();
				RaycastHit hit;
				if (Physics.Raycast (start, forward, out hit, viewDistance)) 
				{
					Gizmos.DrawLine (start, hit.point);
				} 
				else 
				{
					Gizmos.DrawLine (start, start + forward * viewDistance);
				}
				ca += la;
			}

			Gizmos.DrawSphere(navAgent.destination, 0.4f);
		}

		if(investigatePos != Vector3.zero)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(investigatePos, 1f);
		}

		if(chasePosition != Vector3.zero)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(chasePosition, 1f);
		}
	}
	private void drawPatrolPath()
	{
		Gizmos.color = Color.blue;
		if(searchNodes.Count > 0)
		{
			for(int i = 0; i < searchNodes.Count - 1; i++)
			{
				Gizmos.DrawLine(searchNodes[i].transform.position, searchNodes[i + 1].transform.position);
			}
			if(searchNodes.Count > 1)
			{
				//Gizmos.DrawLine(searchNodes[searchNodes.Count - 1].transform.position, searchNodes[0].transform.position);
			}
		}
		
		
	}
	public void seeThrow(GameObject go)
	{
		ms = go.GetComponent<MotionSensor>();

	}
}
