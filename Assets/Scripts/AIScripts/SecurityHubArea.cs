﻿using UnityEngine;
using System.Collections;

public class SecurityHubArea : MonoBehaviour 
{
	private GuardEnemy enemy;

	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals("Enemy"))
		{
			enemy = col.GetComponent<GuardEnemy>();
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.tag.Equals("Enemy") && enemy)
		{
			enemy = null;
		}
	}
	public GuardEnemy getEnemy()
	{
		return enemy;
	}
}
