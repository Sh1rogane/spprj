using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour 
{
//	private CharacterController cc;

	private NavMeshAgent navAgent;
	
	private float fieldOfView = 150f;
	private float viewDistance = 20f;

	//==================================
	//*Idle, Move, Investigate, Arrest *
	//==================================
	// ?
	//
	//
	//
	//
	//
	//
	//
	//

	private enum MainStates {Idle, Patrolling, SuspectPosition, Investigate, Arrest, Search};
	private enum SubStates {Move};
	private enum AlertStates {None, Low, High};

	private MainStates mainState = MainStates.Patrolling;
//	private AlertStates alertState = AlertStates.None;

	[SerializeField]
	private List<Node> nodes = new List<Node>();

	private int currentNodeIndex = 0;

	//Detecion things
	private float detectedMeter;
	private float suspectTimer = 0f;
	private float lostTimer = 0f;

	//Predict stuff
	private Vector3 playerLastSeen = Vector3.zero;
	private Vector3 playerLastSeenPrev = Vector3.zero;
	private Vector3 playerVelocity = Vector3.zero;
	private Vector3 playerPredicted = Vector3.zero;
	private int predictedFrames = 0;

	private Vector3 searchCenter = Vector3.zero;
	private float searchRadius = 20f;
	private Vector3 searchNode = Vector3.zero;
	private float nextNode = 3f;
	private float searchTimer = 0f;

	private float stayTimer = 0f;

	private GameObject player;

	private Transform head;
	private Animator ani;

	private Vector3 idlePosition = Vector3.zero;
	private Quaternion idleRotation;

	// Use this for initialization
	void Start () 
	{
		//cc = this.GetComponent<CharacterController> ();
		navAgent = this.GetComponent<NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag("Player");

		head = transform.Find("Robot/Head");
		ani = GetComponentInChildren<Animator>();



		if(nodes.Count == 0)
		{
			idlePosition = transform.position;
			idleRotation = transform.rotation;
		}
	}
	public List<Node> getNodes()
	{
		return nodes;
	}
	public void addNode(Node node)
	{
		nodes.Add(node);
	}
	public void removeNode(Node node)
	{
		nodes.Remove(node);
	}
	// Update is called once per frame
	void Update () 
	{

	}
	void FixedUpdate()
	{
		ani.SetFloat("Speed", navAgent.velocity.magnitude);
		//print (mainState);
		if(mainState == MainStates.Patrolling)
		{
			patrolling();
		}
		else if(mainState == MainStates.Idle)
		{
			idle ();
		}
		else if(mainState == MainStates.SuspectPosition)
		{
			suspectPosition();
		}
		else if(mainState == MainStates.Investigate)
		{
			investigate();
		}
		else if(mainState == MainStates.Arrest)
		{
			arrest();
		}
		else if(mainState == MainStates.Search)
		{
			search();
		}
		/*if(subState == SubStates.Detecting)
		{
			detecting();
		}*/
	}
	void idle()
	{
		if(navAgentDone())
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, idleRotation, Time.fixedDeltaTime);
		}
		if(detecting())
		{
			mainState = MainStates.SuspectPosition;
			suspectTimer = 1f;
		}
	}
	void search()
	{
		//Set search node to somewhere inside the radius
		if(searchNode == Vector3.zero)
		{
			Vector2 temp = Random.insideUnitSphere * searchRadius;
			searchNode = searchCenter + (new Vector3(temp.x, 0, temp.y));
		}
		else
		{
			nextNode -= Time.fixedDeltaTime;
			if(nextNode <= 0)
			{
				nextNode = 3f;
				Vector2 temp = Random.insideUnitSphere * searchRadius;
				searchNode = searchCenter + (new Vector3(temp.x, 0, temp.y));
			}
		}
		navAgent.SetDestination(searchNode);

		//If enemy can't find the player within 30 second, return to patrolling ("I can't find him" or "He must have fled" or "I give up")
		searchTimer += Time.fixedDeltaTime;
		if(searchTimer > 30f)
		{
			detectedMeter = 0f;
			searchTimer = 0f;
			lostTimer = 0f;
			navAgent.speed = 3.5f;
			searchNode = Vector3.zero;
			searchCenter = Vector3.zero;
			mainState = MainStates.Patrolling;

			if(idlePosition != Vector3.zero)
			{
				mainState = MainStates.Idle;
				navAgent.SetDestination(idlePosition);
			}
		}

		if(detecting())
		{
			mainState = MainStates.Arrest;
		}
	}
	void arrest()
	{
		//"Run" towards the player
		navAgent.speed = 7;

		//If not seeing the player
		if(!detecting())
		{
			//Predict players future position in a linary way
			predictedFrames++;
			playerPredicted = playerLastSeen + playerVelocity * Time.fixedDeltaTime * predictedFrames;

			navAgent.SetDestination(playerPredicted);

			//Increase the lost timer and if it bigger than 5 start searching ("I lost him!")
			lostTimer += Time.fixedDeltaTime;
			if(lostTimer > 5f)
			{
				lostTimer = 0f;
				searchCenter = playerPredicted;
				playerLastSeen = Vector3.zero;
				mainState = MainStates.Search;
			}
		}
		else
		{
			navAgent.SetDestination(playerLastSeen);
			predictedFrames = 0;
			lostTimer = 0f;
		}
	}
	void suspectPosition()
	{
		//Stop the enemy
		navAgent.Stop();

		//Rotate towards the last seen player position
		Vector3 direction = (playerLastSeen - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.fixedDeltaTime * 1f);

		//If enemy sees player
		if(detecting())
		{
			suspectTimer = 1f;
			//Increase detected meter if bigger than x value explore that position/found the player ("Someone was there!")
			detectedMeter += Time.fixedDeltaTime;
			if(detectedMeter > 2f)
			{
				//detectedMeter = 0f;
				mainState = MainStates.Investigate;
				navAgent.SetDestination(playerLastSeen);
			}
		}
		//If enemy not sees the player
		else
		{
			//Lower suspect timer
			suspectTimer -= Time.fixedDeltaTime;
			//If the suspect time is lower than 0 and the detected meter is lower than 1, return to patrolling ("It was nothing")
			if(suspectTimer <= 0f && detectedMeter < 1f)
			{
				detectedMeter = 0f;
				mainState = MainStates.Patrolling;
				playerLastSeen = Vector3.zero;
				if(idlePosition != Vector3.zero)
				{
					mainState = MainStates.Idle;
					navAgent.SetDestination(idlePosition);
				}
			}
			//Else if suspect time is lower that 0 and detect timer is bigger that 1, explore the last known position ("I think i saw something i explore")
			else if(suspectTimer <= 0f && detectedMeter >= 1f)
			{
				detectedMeter = 0f;
				mainState = MainStates.Investigate;
				navAgent.SetDestination(playerLastSeen);
			}
		}

	}
	void investigate()
	{
		//If the enemy detects while exploring
		if(detecting())
		{
			detectedMeter += Time.deltaTime;
			//Detected ("Hey you!" *insert MGS detected sound*)
			if(detectedMeter > 2f)
			{
				this.GetComponent<AudioSource>().Play();
				print ("detected!");
				mainState = MainStates.Arrest;
			}
			stayTimer = 0f;
		}
		//When the nav is done
		if(navAgentDone())
		{
			//Stay there for 3 seconds
			stayTimer += Time.fixedDeltaTime;
			if(stayTimer > 3f)
			{
				//Return to patrolling ("Guess it was nothing")
				stayTimer = 0f;
				mainState = MainStates.Patrolling;
				playerLastSeen = Vector3.zero;
				if(idlePosition != Vector3.zero)
				{
					mainState = MainStates.Idle;
					navAgent.SetDestination(idlePosition);
				}
			}
		}

	}
	void patrolling()
	{
		//Sets the destination to the current node
		if(nodes.Count > 0)
			navAgent.SetDestination(nodes[currentNodeIndex].transform.position);

		//If enemy detects the player (a raycast hits) ("Is something there")
		if(detecting())
		{
			//Change state to suspect position
			mainState = MainStates.SuspectPosition;
			suspectTimer = 1f;
		}
	}
	bool detecting()
	{
		if (fieldOfViewCheck())
		{
			if(raycastToPlayer())
			{
				playerLastSeen = player.transform.position;

				playerVelocity = (playerLastSeen - playerLastSeenPrev) / Time.fixedDeltaTime;

				playerLastSeenPrev = playerLastSeen;
				return true;
			}
		}
		return false;
	}

	bool raycastToPlayer()
	{
		Vector3 targetDir = (player.transform.position) - head.position;
		RaycastHit hit;
		
		if (Physics.Raycast(head.position, targetDir, out hit, viewDistance)) 
		{
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				Debug.DrawLine(head.position, hit.point, Color.red, 0.1f);
				return true;
			}
			Debug.DrawLine(head.position, hit.point, Color.green, 0.1f);
		}
		return false;
	}
	bool raycastToRandom()
	{
		Vector3 targetDir = (player.transform.position + getRandom()) - transform.position;
		RaycastHit hit;

		if (Physics.Raycast(transform.position, targetDir, out hit, viewDistance)) 
		{

			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				Debug.DrawLine(transform.position, hit.point, Color.red, 0.1f);
				return true;
			}
			Debug.DrawLine(transform.position, hit.point, Color.green, 0.1f);
		}
		return false;
	}
	Vector3 getRandom()
	{
		return new Vector3(Random.Range(-1f,1f), Random.Range(-1f,1f), Random.Range(-1f,1f));
	}
	bool fieldOfViewCheck()
	{		
		Vector3 targetDir = player.transform.position - head.position;
		float a = Vector3.Angle(targetDir, head.forward);
		float dist = Vector3.Distance(head.position, player.transform.position);

		if((a < fieldOfView / 2 && dist < viewDistance))
		{
			return true;
		}
		return false;
	}

	bool navAgentDone()
	{
		//if (!navAgent.pathPending)
		//{
			if (navAgent.remainingDistance <= navAgent.stoppingDistance)
			{
				if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude == 0f)
				{
					return true;
				}
			}
		//}
		return false;
	}
	void heardSoundThroughPath(Vector3 pos)
	{
		//("Huh? I heard something")
		heardSound(pos);
	}
	void heardSoundThroughWall(Vector3 pos)
	{
		//("Huh? I heard something")
		heardSound(pos);
	}
	void heardSound(Vector3 pos)
	{
		if(mainState == MainStates.Patrolling || mainState == MainStates.Investigate || mainState == MainStates.SuspectPosition)
		{
			playerLastSeen = pos;
			mainState = MainStates.Investigate;
			navAgent.SetDestination(playerLastSeen);
			suspectTimer = 2f;
		}
		else if(mainState == MainStates.Arrest)
		{
			playerLastSeen = pos;
		}
		else if(mainState == MainStates.Search)
		{
			searchNode = pos;
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals("Node"))
		{
			Node n = col.gameObject.GetComponent<Node>();
			if(nodes[currentNodeIndex] == n)
			{
				currentNodeIndex++;
				if(currentNodeIndex >= nodes.Count)
				{
					currentNodeIndex = 0;
				}
			}
		}
	}
	void OnDrawGizmosSelected()
	{
		drawView ();
		drawPatrolPath();
	}
	private void drawPatrolPath()
	{
		Gizmos.color = Color.blue;
		if(nodes.Count > 0)
		{
			for(int i = 0; i < nodes.Count - 1; i++)
			{
				Gizmos.DrawLine(nodes[i].transform.position, nodes[i + 1].transform.position);
			}
			if(nodes.Count > 1)
			{
				Gizmos.DrawLine(nodes[nodes.Count - 1].transform.position, nodes[0].transform.position);
			}
		}


	}
	private void drawView()
	{
		Gizmos.color = Color.magenta;
		if(head != null)
		{
			Vector3 start = head.position;
		
			float halfAngle = fieldOfView / 2f;

			float ca = -halfAngle;
			float l = fieldOfView / 10f;
			float la = fieldOfView / l;
			for(int i = 0; i < l; i++)
			{
				Quaternion q1 = Quaternion.Euler (head.rotation.eulerAngles.x, head.rotation.eulerAngles.y + ca, head.rotation.eulerAngles.z);
				Vector3 forward = q1 * Vector3.forward;
				forward.Normalize ();
				RaycastHit hit;
				if (Physics.Raycast (start, forward, out hit, viewDistance)) 
				{
					Gizmos.DrawLine (start, hit.point);
				} 
				else 
				{
					Gizmos.DrawLine (start, start + forward * viewDistance);
				}
				ca += la;
			}

			if(playerLastSeen != Vector3.zero)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawWireSphere(playerLastSeen, 1f);
			}
			if(playerPredicted != Vector3.zero)
			{
				Gizmos.color = Color.green;
				Gizmos.DrawWireSphere(playerPredicted, 1f);
			}
			if(searchCenter != Vector3.zero)
			{
				Gizmos.color = Color.yellow;
				Gizmos.DrawWireSphere(searchCenter, searchRadius);
				if(searchNode != Vector3.zero)
				{
					Gizmos.DrawWireSphere(searchNode, 1f);
				}
			}
		}
	}

}
