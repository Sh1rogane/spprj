﻿using UnityEngine;
using System.Collections;

public class SecurityCamera : MonoBehaviour 
{
	private SecurityCameraVision scv;
	// Use this for initialization
	void Start () 
	{
		scv = GetComponentInChildren<SecurityCameraVision>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

	}

	public bool playerInside()
	{
		return scv.playerInside();
	}

	public void disable()
	{
		scv.disable();
	}
}
