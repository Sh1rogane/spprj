﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SecurityHub : MonoBehaviour 
{
	private List<GuardEnemy> enemies = new List<GuardEnemy>();

	private SecurityHubArea sha;

	public SecurityCamera[] cameras;
	// Use this for initialization
	void Start () 
	{
		GameObject[] gos =  GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject go in gos)
		{
			enemies.Add(go.GetComponent<GuardEnemy>());
		}
		sha = GetComponentInChildren<SecurityHubArea>();
	}

	void FixedUpdate()
	{
		GuardEnemy enemy = sha.getEnemy();
		if(enemy != null)
		{
			SecurityCamera sc = checkCameras();
			if(sc != null)
			{
				//Enemy is at security hub and one of the cameras can se the player
				//print("can see you");
				//enemy.cameraChase();
				enemyClosest(GameObject.FindGameObjectWithTag("Player").transform.position).cameraChase();
			}
		}
	}
	private SecurityCamera checkCameras()
	{
		foreach(SecurityCamera sc in cameras)
		{
			if(sc.playerInside())
			{
				return sc;
			}
		}
		return null;
	}
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	private GuardEnemy enemyClosest(Vector3 position)
	{
		GuardEnemy close = null;
		float closeDist = 0;
		
		foreach(GuardEnemy e in enemies)
		{
			if(close == null)
			{
				close = e;
				closeDist = calculatePathLength(close.transform.position, position);
			}
			else
			{
				float dist2 = calculatePathLength(e.transform.position, position);
				
				if(dist2 < closeDist)
				{
					close = e;
					closeDist = dist2;
				}
			}
		}
		return close;
	}
	float calculatePathLength (Vector3 startPosition, Vector3 targetPosition)
	{
		// Create a path and set it based on a target position.
		NavMeshPath path = new NavMeshPath();
		NavMesh.CalculatePath(startPosition, targetPosition, -1, path);
		
		// Create an array of points which is the length of the number of corners in the path + 2.
		Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];
		
		// The first point is the enemy's position.
		allWayPoints[0] = startPosition;
		
		// The last point is the target position.
		allWayPoints[allWayPoints.Length - 1] = targetPosition;
		
		// The points inbetween are the corners of the path.
		for(int i = 0; i < path.corners.Length; i++)
		{
			allWayPoints[i + 1] = path.corners[i];
		}
		
		// Create a float to store the path length that is by default 0.
		float pathLength = 0;
		
		// Increment the path length by an amount equal to the distance between each waypoint and the next.
		for(int i = 0; i < allWayPoints.Length - 1; i++)
		{
			pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
		}
		
		return pathLength;
	}
}
