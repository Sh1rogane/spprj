﻿using UnityEngine;
using System.Collections;

public class SecurityCameraVision : MonoBehaviour 
{

	private bool inside;
	public bool active = true;

	public bool playerInside()
	{
		return inside;
	}

	void OnTriggerEnter(Collider col)
	{
		if(active && col.tag.Equals("Player"))
		{
			inside = true;
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(active && col.tag.Equals("Player") && inside)
		{
			inside = false;
		}
	}

	public void disable()
	{
		active = false;
		GetComponent<MeshRenderer>().enabled = false;
	}
}
