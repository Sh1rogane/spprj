﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialPopup : MonoBehaviour {

	private Text helpText; 
	private Image panel;
	private Image button;

	private UI ui;
	// Use this for initialization
	void Start () 
	{
		helpText = transform.Find ("HelpText").GetComponent<Text>();
		button = transform.Find ("Button").GetComponent<Image>();
		panel = GetComponent<Image>();
		ui = GameObject.Find("UI").GetComponent<UI>();
		setVisibility(false);
	}
	public void showHelp(string text)
	{
		setVisibility(true);
		helpText.text = text;
		//Screen.lockCursor = false;
		ui.popup = true;
	}
	private void setVisibility(bool visibility)
	{
		panel.enabled = visibility;
		helpText.enabled = visibility;
		button.enabled = visibility;
		button.GetComponentInChildren<Text>().enabled = visibility;
	}
	void Update()
	{
		if(Input.GetKeyUp(KeyCode.Return))
		{
			Time.timeScale = 1;
			setVisibility(false);
			//Screen.lockCursor = true; 
			ui.popup = false;
		}
	}
	public void onButtonClick()
	{
		Time.timeScale = 1;
		setVisibility(false);
		//Screen.lockCursor = true; 
		ui.popup = false;
	}


}
