﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour 
{
	private UI ui;


	void Start()
	{
		ui = transform.parent.GetComponent<UI>();
	}


	public void onResume()
	{
		ui.showPauseMenu(false);
	
	}
	public void onRestart()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	public void onObjective()
	{

	}
	public void onQuit()
	{
		Application.LoadLevel("Menu");
	}
}
