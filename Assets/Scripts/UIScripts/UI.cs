﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class UI : MonoBehaviour 
{
	public static UI instance;

	private bool showing;

	public bool popup;

	public Camera sc;

	private RawImage cameraImage;

	private Animator ani;
	private Animator minimapAni;
	private Animator deathFadeAni;

	private bool minimapMax;
	
	private Camera sensorCamera;

	private Player player;

	private Text motionSensorText;
	private Text cameraText;
	private Text noiseMakerText;
	private Text noText;

	private Slider lightSlider;

	private InteractUI interactUI;

	private int currentCameraIndex = -1;

	public bool showingPauseMenu;
	private GameObject pauseMenu;

	private BlurOptimized blur;

	private CanvasGroup cg;

	private Text checkpointText;

	// Use this for initialization
	void Start () 
	{
		instance = this;

		cameraImage = this.GetComponentInChildren<RawImage>();
		cg = GetComponent<CanvasGroup>();

		ani = GetComponentInChildren<Animator>();
		//mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		sensorCamera = GameObject.Find("Sensor Camera").GetComponent<Camera>();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		motionSensorText = transform.Find("MainPanel/ItemPanel/MotionSensorText").gameObject.GetComponent<Text>();
		cameraText = transform.Find("MainPanel/ItemPanel/CameraText").gameObject.GetComponent<Text>();
		noiseMakerText = transform.Find("MainPanel/ItemPanel/NoiseMakerText").gameObject.GetComponent<Text>();

		noText = transform.Find("MainPanel/CameraPanel/NoSignal").GetComponent<Text>();

		lightSlider = transform.Find("MainPanel/LightPanel/Slider").gameObject.GetComponent<Slider>();

		interactUI = transform.Find("InteractUI").GetComponent<InteractUI>();

		minimapAni = GameObject.Find("MinimapCamera").GetComponent<Animator>();

		deathFadeAni = transform.Find("DeathFade").GetComponent<Animator>();

		pauseMenu = transform.Find("PauseMenu").gameObject;

		checkpointText = transform.Find("CheckpointText").GetComponent<Text>();
		checkpointText.enabled = false;

		blur = player.transform.Find("ArmCamera").GetComponent<BlurOptimized>();
		blur.enabled = false;

		showPauseMenu(false);

		cg.blocksRaycasts = false;
	}
	public void showPauseMenu(bool visibility)
	{
		showingPauseMenu = visibility;
		pauseMenu.SetActive(visibility);

		if(showingPauseMenu)
		{
			cg.blocksRaycasts = true;
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
			Time.timeScale = 0f;
		}
		else
		{
			cg.blocksRaycasts = false;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			if(!popup)
			{
				Time.timeScale = 1f;
			}
		}
	}
	public void showCheckpoint()
	{
		checkpointText.enabled = true;
		checkpointText.color = Color.white;
	}
	public void die()
	{
		deathFadeAni.SetTrigger("die");
	}
	public void setInteractUI(bool value)
	{
		interactUI.setVisible(value);
	}
	public void setLightSlider(float value)
	{
		lightSlider.value = value;
	}
	// Update is called once per frame
	void Update () 
	{
		if(checkpointText.enabled)
		{
			Color c = checkpointText.color;
			c.a -= Time.deltaTime * 0.33f;
			checkpointText.color = c;
			if(checkpointText.color.a <= 0)
			{
				checkpointText.enabled = false;
			}
		}
		if(Input.GetKeyDown(KeyCode.Tab))
		{
			showing = !showing;
			ani.SetBool("open", showing);
		}
		if(showing)
		{
			blur.enabled = true;
			blur.blurSize = Mathf.Lerp(blur.blurSize, 3, Time.deltaTime * 3);
		}
		if(showing && ani.GetCurrentAnimatorStateInfo(0).IsName("New Animation") && ani.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
		{
			sensorCamera.enabled = true;
		}
		else if(!showing)
		{
			sensorCamera.enabled = false;
			blur.blurSize = Mathf.Lerp(blur.blurSize, 0, Time.deltaTime * 3);
			if(blur.blurSize < 0.5f)
			{
				blur.enabled = false;
			}
		}

		if(Input.GetKeyDown(KeyCode.K))
		{
			setTexture(currentCameraIndex - 1);
		}
		else if(Input.GetKeyDown(KeyCode.L))
		{
			setTexture(currentCameraIndex + 1);
		}

		if(Input.GetKeyDown(KeyCode.M))
		{
			minimapMax = !minimapMax;

			ani.SetBool("maximized", minimapMax);
			minimapAni.SetBool("maximized", minimapMax);
		}
	}
	void FixedUpdate()
	{
		motionSensorText.color = Color.black;
		cameraText.color = Color.black;
		noiseMakerText.color = Color.black;

		if(player.getGadgetIndex() == Player.MOTIONSENSOR)
		{
			motionSensorText.color = Color.red;
		} 
		else if(player.getGadgetIndex() == Player.GADGETCAMERA)
		{
			cameraText.color = Color.red;
		} 
		else 
		{
			noiseMakerText.color = Color.red;
		}
		motionSensorText.text = "M " + player.motionSensor;
		cameraText.text = "C " + player.gadgetCamera;
		noiseMakerText.text = "N " + player.noiseMaker;
	}
	public void setTexture(int pos)
	{
		if(currentCameraIndex >= 0 && currentCameraIndex < GadgetCamera.cameras.Count)
			GadgetCamera.cameras[currentCameraIndex].unselect();
	

		currentCameraIndex = pos;

		if(currentCameraIndex > GadgetCamera.cameras.Count - 1)
		{
			currentCameraIndex = 0;

		} else if(currentCameraIndex < 0)
		{
			currentCameraIndex = GadgetCamera.cameras.Count - 1;
		}


		if(GadgetCamera.cameras.Count == 0)
		{
			cameraImage.texture = null;
			noText.enabled = true;
		}
		else
		{
			noText.enabled = false;
			cameraImage.texture = GadgetCamera.cameras[currentCameraIndex].renderTexture;
			GadgetCamera.cameras[currentCameraIndex].select();
		}

	}
}
