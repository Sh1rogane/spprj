﻿using UnityEngine;
using System.Collections;

public class HelpDisplay : MonoBehaviour {

	private TutorialPopup tp;
	public string text;

	private bool used;

	// Use this for initialization
	void Start () {
	
		tp = GameObject.Find ("TutorialPopup").GetComponent<TutorialPopup>();
	}

	void OnTriggerEnter(Collider col)
	{
		//text appears
		if(!used && col.tag.Equals("Player") && !Global.tutorialDisable)
		{
			used = true;
			Time.timeScale = 0;
			tp.showHelp(text);
		}

	}
}
