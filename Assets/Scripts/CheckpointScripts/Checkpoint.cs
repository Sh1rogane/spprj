﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {

	public int index = 0;

	private UI ui;

	private bool used;
	// Use this for initialization
	void Start () 
	{
		ui = GameObject.Find("UI").GetComponent<UI>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals("Player") && !used)
		{
			used = true;
			ui.showCheckpoint();
			CheckpointSystem.checkpointIndex = index;
		}

	}
}
