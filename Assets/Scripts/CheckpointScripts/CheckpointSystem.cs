﻿using UnityEngine;
using System.Collections;

public class CheckpointSystem : MonoBehaviour {

	public static int checkpointIndex = 0;

	//Checkpoint 1
	public Checkpoint cp1;
	public SecurityCamera cam1;
	public Keypad keypad1;
	public Laser laser1;
	public Door door1;
	public Lvl3Hack1 hack1;
	public Room9Hack hack2;
	public Room10Hack hack3;
	//Checkpoint 2
	public Checkpoint cp2;
	public SecurityCamera cam2;
	public Keypad keypad2;
	public Door door2;
	public Room13Hack hack4;

	// Use this for initialization
	void Start () 
	{
		if(checkpointIndex == -1)
		{
			Application.LoadLevel("menu");
		}
		if(checkpointIndex == 1)
		{
			checkpoint1();
			Player.instance.transform.position = cp1.transform.position;
		}
		else if(checkpointIndex == 2)
		{
			checkpoint1();
			checkpoint2();
			Player.instance.transform.position = cp2.transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void checkpoint1()
	{
		//cam1.disable();
		//keypad1.open();
		//laser1.disable();
		door1.openDoor();
		hack1.hackedSucceeded();
		hack2.hackedSucceeded();
		hack3.hackedSucceeded();
	}
	public void checkpoint2()
	{
		//cam2.disable();
		//keypad2.open();
		door2.openDoor();
		hack4.hackedSucceeded();
	}
}
