﻿using UnityEngine;
using System.Collections;

public class SoundInstance : MonoBehaviour 
{
	public static Object prefab = Resources.Load("SoundInstance");

	public GameObject owner;
	private float power;

	private float scale = 0;

	public static SoundInstance create(GameObject owner, Vector3 pos, float power)
	{
		GameObject newObject = Instantiate(prefab) as GameObject;
		SoundInstance instance = newObject.GetComponent<SoundInstance>();
		
		instance.owner = owner;
		instance.power = power;
		instance.transform.position = pos;
		
		return instance;
	}
	// Use this for initialization
	void Start () 
	{
		transform.localScale = Vector3.zero;
	}
	void FixedUpdate()
	{
		scale += 1;
		transform.localScale = new Vector3(scale, scale, scale);
		if(scale >= power)
		{
			Destroy(gameObject);
		}
	}
	public float getPower()
	{
		return power;
	}
	// Update is called once per frame
	void Update () 
	{
	
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, scale);
	}
}
