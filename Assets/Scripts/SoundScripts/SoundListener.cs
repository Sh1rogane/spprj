﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SoundListener : MonoBehaviour 
{
	private Vector3[] allWayPoints;
	// Use this for initialization
	void Start () 
	{
		//nav = this.GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	float calculatePathLength (Vector3 targetPosition)
	{
		// Create a path and set it based on a target position.
		NavMeshPath path = new NavMeshPath();
		NavMesh.CalculatePath(transform.position, targetPosition, -1, path);
		
		// Create an array of points which is the length of the number of corners in the path + 2.
		allWayPoints = new Vector3[path.corners.Length + 2];
		
		// The first point is the enemy's position.
		allWayPoints[0] = transform.position;
		
		// The last point is the target position.
		allWayPoints[allWayPoints.Length - 1] = targetPosition;
		
		// The points inbetween are the corners of the path.
		for(int i = 0; i < path.corners.Length; i++)
		{
			allWayPoints[i + 1] = path.corners[i];
		}
		
		// Create a float to store the path length that is by default 0.
		float pathLength = 0;
		
		// Increment the path length by an amount equal to the distance between each waypoint and the next.
		for(int i = 0; i < allWayPoints.Length - 1; i++)
		{
			pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
		}
		
		return pathLength;
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals("SoundInstance"))
		{
			SoundInstance si = col.gameObject.GetComponent<SoundInstance>();

			float soundPower = si.getPower();

			float pathDistance = calculatePathLength(col.gameObject.transform.position);
			//print("pathDistance " + pathDistance);
			if(pathDistance <= soundPower)
			{
				//Heard the sound by the walkway
				this.gameObject.SendMessage("heardSoundThroughPath", si.transform.position);
			}
			/*else
			{
				Vector3 targetDir = col.gameObject.transform.position - transform.position;
				float dist = Vector3.Distance(col.gameObject.transform.position, transform.position);
				
				LayerMask lm = 1 << 8;
				lm = ~lm;
				RaycastHit[] hit = Physics.RaycastAll(transform.position, targetDir, dist, lm);
				hit = hit.OrderBy(item => item.distance).ToArray();
				
				float rayDistance = soundPower - dist;
				
				foreach(RaycastHit rh in hit)
				{
					SoundAbsorber sa = rh.collider.gameObject.GetComponent<SoundAbsorber>();
					if(sa != null)
					{
						rayDistance -= sa.absorption;
					}
				}
				print("ray distance " + rayDistance);
				if(rayDistance > 0)
				{
					//Heard the sound through a wall
					this.gameObject.SendMessage("heardSoundThroughWall", si.transform.position);
				}
			}*/

		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		if(allWayPoints != null)
		{
			for(int i = 0; i < allWayPoints.Length - 1; i++)
			{
				Gizmos.DrawLine(allWayPoints[i], allWayPoints[i + 1]);
			}
		}
	}

}
