﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
	public static void addSound(GameObject owner, Vector3 pos, float power)
	{
		SoundInstance.create(owner, pos, power);
	}
	public static void emitSound(Vector3 position, float power)
	{
		Vector3 startDir = Vector3.forward;
		Vector3 normal = Vector3.forward;
		Vector3 startPos = position;
		
		float steps = 360;
		float step = 360 / steps;
		
		for(int test = 0; test < steps + 1; test++)
		{
			SoundPath sp = new SoundPath();
			float p = power;
			for(int i = 0; i < 10; i++)
			{
				RaycastHit hit;
				if (Physics.Raycast(startPos, normal, out hit, p)) 
				{
					if(hit.collider.gameObject.tag.Equals("Enemy"))
					{
						sp.addPath(startPos, hit.point);
						sp.hitEnemy = true;
						break;
					}
					else
					{
						sp.addPath(startPos, hit.point);
					}
					p -= hit.distance;
					normal = Vector3.Reflect(normal, hit.normal);
					startPos = hit.point;
				}
				else
				{
					sp.addPath(startPos, startPos + normal * p);
					break;
				}
			}
			startPos = position;
			startDir = Quaternion.AngleAxis(test * step, Vector3.up) * Vector3.forward;
			normal = startDir;
			sp.drawPath();
		}
	}
	public class SoundPath
	{
		List<TwoVectors> list = new List<TwoVectors>();

		public bool hitEnemy;

		public SoundPath()
		{

		}
		public void addPath(Vector3 start, Vector3 end)
		{
			list.Add(new TwoVectors(start, end));
		}
		public void drawPath()
		{
			if(hitEnemy)
			{
				foreach(TwoVectors tv in list)
				{
					Debug.DrawLine(tv.start, tv.end, Color.red);
				}
			}
			else
			{
				foreach(TwoVectors tv in list)
				{
					Debug.DrawLine(tv.start, tv.end, Color.blue);
				}
			}
		}
		public class TwoVectors
		{
			public Vector3 start;
			public Vector3 end;

			public TwoVectors(Vector3 start, Vector3 end)
			{
				this.start = start;
				this.end = end;
			}
		}
	}
}
