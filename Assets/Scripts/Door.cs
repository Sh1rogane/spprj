﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	private Animator animator;

	public bool open;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();

		if(open)
		{
			openDoor();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void openDoor()
	{
		animator.SetBool("open", true);
	}
	public void closeDoor()
	{
		animator.SetBool("open", false);
	}
}
