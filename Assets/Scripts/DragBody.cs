﻿using UnityEngine;
using System.Collections;

public class DragBody : MonoBehaviour 
{

	private Rigidbody dragBody;

	private Player player;
	private FixedJoint joint;
	// Use this for initialization
	void Start () 
	{
		dragBody = transform.Find("Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow").GetComponent<Rigidbody>();

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		joint = player.GetComponentInChildren<FixedJoint>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(joint.connectedBody)
		{
			dragBody.position = joint.transform.position;
		}
	}

	void interactUse()
	{
		if(!joint.connectedBody)
		{
			joint.connectedBody = dragBody;
			dragBody.useGravity = false;
			dragBody.position = joint.transform.position;
		}
		else
		{
			dragBody.useGravity = true;
			joint.connectedBody = null;
		}
	}
}
