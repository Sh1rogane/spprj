﻿using UnityEngine;
using System.Collections;

public class TestAnimation : MonoBehaviour {

	private Animator anim;
	private NavMeshAgent navAgent;

	public Transform target;
	public AudioClip footstep;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		navAgent = GetComponent<NavMeshAgent>();
		//navAgent.updatePosition = false;
		Invoke("startWalk", 3f);
	}
	void startWalk()
	{
		navAgent.SetDestination(target.position);
		
		anim.SetFloat("speed", 1f);
	}

	void playSound()
	{
		GetComponent<AudioSource>().PlayOneShot (footstep);
	}

	// Update is called once per frame
	void Update () {
		print (navAgent.remainingDistance);
		if(navAgent.remainingDistance <= 0)
		{
			anim.SetFloat("speed", 0f);
		}
	}
}
