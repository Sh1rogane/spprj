﻿using UnityEngine;
using System.Collections;

public class NoiseMaker : MonoBehaviour 
{
	private float soundTimer;

	private AudioSource sound;
	// Use this for initialization
	void Start () 
	{
		sound = GetComponent<AudioSource>();
		Invoke("playSound", 4.5f);
	}
	public void playSound()
	{
		sound.Play();
	}
	// Update is called once per frame
	void Update () 
	{

		soundTimer += Time.deltaTime;
		if(soundTimer >= 5f)
		{
			soundTimer = 0f;
			SoundManager.addSound(this.gameObject, transform.position, 20f);

			Destroy(this.gameObject);
		}
	}
}
