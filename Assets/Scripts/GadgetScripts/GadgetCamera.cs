﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GadgetCamera : MonoBehaviour {

	public static List<GadgetCamera> cameras = new List<GadgetCamera>();

	public RenderTexture renderTexture;

	private SpriteRenderer minimapIcon;

	// Use this for initialization
	void Start () 
	{
		renderTexture = new RenderTexture (512, 512, 24);

		this.GetComponent<Camera>().targetTexture = renderTexture;

		cameras.Add(this);

		minimapIcon = GetComponentInChildren<SpriteRenderer>();

		UI.instance.setTexture(cameras.Count - 1);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void select()
	{
		minimapIcon.color = Color.blue;
	}
	public void unselect()
	{
		minimapIcon.color = Color.green;
	}
	public void interactUse()
	{
		Destroy(gameObject);
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().gadgetCamera++;
		cameras.Remove(this);
		if(GadgetCamera.cameras.Count > 0)
		{
			UI.instance.setTexture(GadgetCamera.cameras.Count - 1);
		}
		else
		{
			UI.instance.setTexture(-1);
		}
	}
}
