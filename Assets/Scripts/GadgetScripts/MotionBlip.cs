﻿using UnityEngine;
using System.Collections;

public class MotionBlip : MonoBehaviour {
	
	public static Object prefab = Resources.Load("MotionBlip");

	public static GameObject createInstance(Vector3 pos)
	{
		return Instantiate(prefab, pos, Quaternion.identity) as GameObject;
	}
	// Use this for initialization
	void Start () {
		Destroy(gameObject, 0.98f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
