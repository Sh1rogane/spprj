﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MotionSensor : MonoBehaviour {


//	private GuardEnemy enemy;
	private static GameObject blip;

	public int viewDistance = 20;
	private SphereCollider sphere;
	public Rigidbody rbody;
	public bool isMoving = true;
	private List<MotionInstance> enemies = new List<MotionInstance>();
	private int currentEnemy;

	// Use this for initialization
	void Start () {
		sphere = this.GetComponent<SphereCollider> ();
		rbody = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isMoving && rbody.IsSleeping())
		{
			isMoving = false;
		}
		if(!isMoving){

			foreach(MotionInstance mi in enemies)
			{
				if(mi.enemyInside)
				{
					mi.enemyInSight = false;
					if(sightCheck(mi.enemy))
					{
						mi.enemyInSight = true;
						mi.enemyLostFrame = false;
						if(!mi.enemyInSightFrame)
						{
							mi.enemyInSightFrame = true;
							mi.gotSignal();
						}
					}
					if(mi.enemyInSight)
					{
						mi.motionTimer += Time.deltaTime;
						if(mi.motionTimer > 2f)
						{
							mi.motionTimer = 0;
							blip = MotionBlip.createInstance(mi.enemy.getRaycastPosition());
						}
					}
					else if(!mi.enemyInSight && mi.enemyInSightFrame)
					{
						mi.enemyInSightFrame = false;
						if(!mi.enemyLostFrame)
						{
							mi.enemyLostFrame = true;
							mi.lostSignal();
						}
					}
				}
			}
		}
	}
	public void interactUse()
	{
		Destroy(gameObject);
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().motionSensor++;
	}
	bool sightCheck(GuardEnemy enemy)
	{
		Vector3 dir = (enemy.getRaycastPosition()) - transform.position;

		RaycastHit hit;

		LayerMask lm = 1 << 12;
		lm |= 1 << 2;
		lm = ~lm;

		if (Physics.Raycast (transform.position, dir, out hit, sphere.radius, lm)) {

			//Debug.Log(hit.collider);
			Debug.DrawLine (transform.position, hit.point, Color.red, 0.1f);
			if (hit.collider.GetComponent<GuardEnemy>() == enemy && (hit.distance < sphere.radius)) 
			{
				return true;
			}

		}
		
		return false;
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals("Enemy"))
		{
			MotionInstance temp = getInstance(col.GetComponent<GuardEnemy>());
			if(temp == null)
			{
				MotionInstance mi = new MotionInstance(col.GetComponent<GuardEnemy>());
				mi.enemyInside = true;
				mi.enemyInSightFrame = false;
				
				enemies.Add(mi);
			}
		} 
	}
	void OnTriggerStay(Collider col)
	{

	}
	void gotSignal(MotionInstance mi)
	{
		//motionTimer = 0f;
		blip = MotionBlip.createInstance (mi.enemy.getRaycastPosition());
		//blip = MotionBlip.createInstance (enemies[currentEnemy].getRaycastPosition());
		blip.GetComponentInChildren<Renderer>().material.color = new Color(255, 71, 0, 0.1f);
		blip.GetComponent<AudioSource>().loop = true;
		blip.GetComponent<AudioSource> ().pitch = 2f;
	}
	void lostSignal(MotionInstance mi)
	{
		blip = MotionBlip.createInstance (mi.enemy.getRaycastPosition());
		//blip = MotionBlip.createInstance (enemies[currentEnemy].getRaycastPosition());
		blip.GetComponentInChildren<Renderer> ().material.color = new Color (255, 0, 0, 0.3f);
		blip.GetComponent<AudioSource> ().loop = true;
		blip.GetComponent<AudioSource> ().pitch = 0.5f;

	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals("Enemy"))
		{
			MotionInstance mi = getInstance(col.GetComponent<GuardEnemy>());
			if(mi != null)
			{
				if(mi.enemyInSight)
				{
					mi.lostSignal();
				}
				mi.enemyLostFrame = false;
				mi.enemyInside = false;
				enemies.Remove(mi);
			}

		}
	}
	private MotionInstance getInstance(GuardEnemy ge)
	{
		foreach(MotionInstance mi in enemies)
		{
			if(mi.enemy.Equals(ge))
			{
				return mi;
			}
		}
		return null;
	}
	private class MotionInstance
	{
		public float motionTimer = 0f;
		public bool enemyInside;
		public bool enemyInSight;
		public bool enemyInSightFrame;
		public bool enemyLostFrame;

		public GuardEnemy enemy;

		public MotionInstance(GuardEnemy enemy)
		{
			this.enemy = enemy;
		}

		public void gotSignal()
		{
			motionTimer = 0f;
			blip = MotionBlip.createInstance (enemy.getRaycastPosition());
			blip.GetComponentInChildren<Renderer>().material.color = new Color(255, 71, 0, 0.1f);
			blip.GetComponent<AudioSource>().loop = true;
			blip.GetComponent<AudioSource> ().pitch = 2f;
		}
		public void lostSignal()
		{
			blip = MotionBlip.createInstance (enemy.getRaycastPosition());
			blip.GetComponentInChildren<Renderer> ().material.color = new Color (255, 0, 0, 0.3f);
			blip.GetComponent<AudioSource> ().loop = true;
			blip.GetComponent<AudioSource> ().pitch = 0.5f;
			
		}
	}
}
