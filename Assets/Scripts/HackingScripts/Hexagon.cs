﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hexagon : MonoBehaviour 
{
	//public int[] correctRotation;
	public int correctSide;
	public bool hasAltCorrect;
	public int altCorrectSide;

	private HackingGame hg;

	private Vector3 startRotation;

	private Button button;

	// Use this for initialization
	void Start () 
	{
		hg = this.GetComponentInParent<HackingGame>();
		button = this.GetComponent<Button>();
		button.interactable = false;
		startRotation = transform.localRotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void Rotate()
	{
		this.transform.Rotate(new Vector3(0,0,-60));
		hg.checkSolution();
	}
	public bool checkRotation()
	{
		if(correctSide * 60 == Mathf.RoundToInt(transform.localRotation.eulerAngles.z))
		{
			return true;
		}
		else if(hasAltCorrect && altCorrectSide * 60 == Mathf.RoundToInt(transform.localRotation.eulerAngles.z))
		{
			return true;
		}
//		foreach(int i in correctRotation)
//		{
//			if(i == Mathf.FloorToInt(transform.localRotation.eulerAngles.z))
//			{
//				return true;
//			}
//		}
		return false;
	}
	public void setActive(bool value)
	{
		button.interactable = value;
	}
	public void reset()
	{
		transform.rotation = Quaternion.Euler(startRotation);
	}
}
