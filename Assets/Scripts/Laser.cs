﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour 
{
	private GameObject l1;
	private GameObject l2;
	private GameObject l3;

	private GameObject hitbox;

	// Use this for initialization
	void Start () 
	{
		l1 = transform.Find("Cylinder").gameObject;
		l2 = transform.Find("Cylinder 1").gameObject;
		l3 = transform.Find("Cylinder 2").gameObject;
		hitbox = transform.Find("Cylinder/Cube").gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void disable()
	{
		l1.SetActive(false);
		l2.SetActive(false);
		l3.SetActive(false);
	}
}
