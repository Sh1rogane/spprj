﻿using UnityEngine;
using System.Collections;

public class LightDome : MonoBehaviour {

	private GameLight gl;
	private Renderer rend;
	// Use this for initialization
	void Start () {
		gl = this.transform.parent.GetComponentInChildren<GameLight>();
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(gl.on)
		{
			rend.material.SetColor("_EmissionColor", Color.white);
			//DynamicGI.SetEmissive(rend, Color.black);
		}
		else
		{
			rend.material.SetColor("_EmissionColor", Color.black);
			//DynamicGI.SetEmissive(rend, Color.white);
		}
	}
}
