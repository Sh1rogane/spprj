﻿using UnityEngine;
using System.Collections;

public class LightSwitch : MonoBehaviour 
{
	public GameLight[] lights;

	public bool dominant = true;

	// Use this for initialization
	void Start () 
	{
		if(dominant)
		{
			foreach(GameLight gl in lights)
			{	
				gl.ls = this;
			}
		}
	}

	public void interactUse()
	{
		foreach(GameLight gl in lights)
		{
			gl.changeState();
		}
	}
	public void enemyUse()
	{
		foreach(GameLight gl in lights)
		{
			gl.changeStateEnemy();
		}
	}
}
