﻿using UnityEngine;
using System.Collections;

public class GameLight : MonoBehaviour 
{
	public bool on;
	public Light l;
	public LightSwitch ls;

	private float startRange;
	// Use this for initialization
	void Start () {
		startRange = GetComponent<Light>().range;

		if(GetComponent<Light>().range > 0)
		{
			on = true;
		}
		else
		{
			on = false;
		}

		l = GetComponent<Light>();
		//changeState(false);
	}
	void FixedUpdate()
	{

	}
	// Update is called once per frame
	void Update () {
	}
	//Changes the state of the light
	public void changeState()
	{
		on = !on;
		if(on)
		{
			GetComponent<Light>().range = startRange;
		}
		else
		{
			GetComponent<Light>().range = 0;
		}
		distanceEnemy();
	}
	public void changeStateEnemy()
	{
		on = !on;
		if(on)
		{
			GetComponent<Light>().range = startRange;
		}
		else
		{
			GetComponent<Light>().range = 0;
		}
	}
	public void interactUse()
	{
		changeState();
	}
	private void distanceEnemy()
	{
		//Find all enemies in scene
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

		foreach(GameObject go in gos)
		{
			float dist = Vector3.Distance(transform.position, go.transform.position);
			if(dist < startRange)
			{
				go.GetComponent<GuardEnemy>().investigateLight(ls);
			}
		
		}
	}
}
