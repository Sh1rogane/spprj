﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour 
{
	private Light flashlight;

	private Player player;

	private bool on = true;

	// Use this for initialization
	void Start () 
	{
		flashlight = GetComponent<Light>();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}



	// Update is called once per frame
	void Update () 
	{

	}
	public void turnOn()
	{
		on = true;
		flashlight.enabled = true;
	}
	public void turnOff()
	{
		on = false;
		flashlight.enabled = false;
	}
	public bool canSeePlayer()
	{
		if(on)
		{
			if(playerInside())
			{
				if(checkRaycast())
				{
					return true;
				}
			}
		}
		return false;
	}
	private bool checkRaycast()
	{
		Vector3 targetDir = (player.getRaycastPosition()) - transform.position;
		RaycastHit hit;
		
		if (Physics.Raycast(transform.position, targetDir, out hit, flashlight.range)) 
		{
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				Debug.DrawLine(transform.position, hit.point, Color.red, 0.1f);
				return true;
			}
			Debug.DrawLine(transform.position, hit.point, Color.green, 0.1f);
		}
		return false;
	}
	private bool playerInside()
	{
		Vector3 targetDir = player.transform.position - transform.position;
		float a = Vector3.Angle(targetDir, transform.forward);
		//float dist = Vector3.Distance(getEyePosition(), player.getRaycastPosition());
		//print (a);
		if((a < flashlight.spotAngle / 2))
		{
			return true;
		}
		return false;
	}
}
