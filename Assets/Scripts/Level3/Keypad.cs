﻿using UnityEngine;
using System.Collections;

public class Keypad : MonoBehaviour 
{
	public Door door;

	private bool canOpen;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	public void open()
	{
		canOpen = true;
		GetComponent<Interactable>().useText = "Use Code";
	}
	public void interactUse()
	{
		if(canOpen)
		{
			door.openDoor();
		}

	}
}
