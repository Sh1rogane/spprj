﻿using UnityEngine;
using System.Collections;

public class Room18Hack : MonoBehaviour 
{
	private Object hgPrefab;
	private GameObject hg;
	private HackingGameV2 hgScript;
	
	private bool canHack = true;
	private Interactable interactable;

	public SecurityCamera cam;
	public Keypad keypad;
	
	//public Laser laser;
	// Use this for initialization
	void Start () 
	{
		hgPrefab = Resources.Load("HackingGame/HackingGame Level 5");
		interactable = GetComponent<Interactable>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	public void interactUse()
	{
		if(canHack)
		{
			createGame();
		}
	}
	private void createGame()
	{
		hg = Instantiate (hgPrefab) as GameObject;
		hgScript = hg.GetComponent<HackingGameV2>();
		hgScript.hackedObject = this.gameObject;
		Invoke("start", 0.1f);
	}
	public void start()
	{
		hgScript.start();
	}
	public void hackedSucceeded()
	{
		cam.disable();
		keypad.open();
		//laser.disable();
		interactable.enabled = false;
		//Ful fix
		Destroy(interactable);
		canHack = false;
	}
	
	public void hackedFailed()
	{
		
	}
}
