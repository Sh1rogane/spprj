﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MinigameTutorial : MonoBehaviour 
{
	//private Text text1;
	private Text text2;
	private Text text3;
	private Text text4;
	private Text text5;
	private Text text6;

	private Text buttonText;

	private int nextClicks = 0;

	private HackingGameV2 hg;

	private bool active;

	// Use this for initialization
	void Start () 
	{
		hg = GetComponentInParent<HackingGameV2>();
		hg.active = false;
		//text1 = transform.Find("Text1").GetComponent<Text>();
		text2 = transform.Find("Text2").GetComponent<Text>();
		text3 = transform.Find("Text3").GetComponent<Text>();
		text4 = transform.Find("Text4").GetComponent<Text>();
		text5 = transform.Find("Text5").GetComponent<Text>();
		text6 = transform.Find("Text6").GetComponent<Text>();

		buttonText = transform.Find("Button/Text").GetComponent<Text>();

		text2.enabled = false;
		text3.enabled = false;
		text4.enabled = false;
		text5.enabled = false;
		text6.enabled = false;

		active = true;

		Player.instance.hacking = true;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
	public void onNext()
	{
		nextClicks++;
		if(nextClicks == 1)
		{
			text2.enabled = true;
		}
		else if(nextClicks == 2)
		{
			text3.enabled = true;
		}
		else if(nextClicks == 3)
		{
			text4.enabled = true;
		}
		else if(nextClicks == 4)
		{
			text5.enabled = true;
		}
		else if(nextClicks == 5)
		{
			text6.enabled = true;
			buttonText.text = "Start";
		}
		else if(nextClicks == 6)
		{
			active = false;
			Invoke("endTutorial", 0.1f);
		}
	}
	public void endTutorial()
	{
		hg.active = true;
		Time.timeScale = 1;
		gameObject.SetActive(false);
	}
	// Update is called once per frame
	void Update () 
	{
		if(active)
		{
			hg.active = false;
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}

	}
}
