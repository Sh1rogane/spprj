﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class HackingGameV2 : MonoBehaviour 
{
	private Text timeLeft;
	private Text attempts;
	private Text fail;
	private Text success;
	private Image middlePanel;
	private MinigamePlayer player;
	private Player p;
	public float countDown;
	public float bruteAttempts;
	public bool active;
	private List<MinigameBit> bits = new List<MinigameBit>(); 
	private Camera gameCamera;

	private bool lost;

	public GameObject hackedObject;

	// Use this for initialization
	void Start () 
	{
		timeLeft = transform.Find("UI/ToppPanel/Time").GetComponent<Text>();
		attempts = transform.Find("UI/BottomPanel/AttemptsRemaining").GetComponent<Text>();
		fail = transform.Find("UI/MiddlePanel/Fail").GetComponent<Text>();
		success = transform.Find("UI/MiddlePanel/Success").GetComponent<Text>();
		middlePanel = transform.Find("UI/MiddlePanel").GetComponent<Image>();
		player = GetComponentInChildren<MinigamePlayer>();
		if(GameObject.FindGameObjectWithTag("Player"))
		{
			p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		}

		middlePanel.enabled = false;
		fail.enabled = false;
		success.enabled = false;
		attempts.text = bruteAttempts.ToString();
		collectBits();
		//active = true;
		gameCamera = GetComponentInChildren<Camera>();
	}
	public void start()
	{
		active = true;
		gameCamera.depth = 1;
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
		p.hacking = true;
	}
	public void end()
	{
		active = false;
		countDown = 15;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		p.hacking = false;
		gameCamera.depth = -10;

		Destroy(gameObject);
	}
	// Update is called once per frame
	void Update () 
	{
		if(active)
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
			Timer ();
			if(bruteAttempts == 0 && !player.isMoving())
			{
				lose();	
			}
		}

		if(lost)
		{
			gameCamera.GetComponent<VignetteAndChromaticAberration>().chromaticAberration = Mathf.Lerp(gameCamera.GetComponent<VignetteAndChromaticAberration>().chromaticAberration, 200, Time.deltaTime);
		}

	}
	public void removeBit(MinigameBit bit)
	{
		bits.Remove(bit);
		if(bits.Count == 0)
		{
			//win
			win ();
		}
	}
	private void lose()
	{
		middlePanel.enabled = true;
		fail.enabled = true;
		fail.GetComponent<Animator>().SetTrigger("play");
		active = false;
		lost = true;
		callFailed();
		Invoke("end", 2f);
	}
	private void win()
	{
		middlePanel.enabled = true;
		success.enabled = true;
		success.GetComponent<Animator>().SetTrigger("play");
		active = false;
		callHackedObject();
		Invoke("end", 2f);
	}
	private void collectBits()
	{
		bits.AddRange(GetComponentsInChildren<MinigameBit>());

	}
	private void Timer()
	{
		countDown -= Time.deltaTime;
		//timeLeft.text = countDown.ToString("s");
		if(countDown <= 0)
		{
			countDown = 0;
			lose ();
		}

		if(countDown < 10)
		{
			timeLeft.text = "0" + Mathf.Floor(countDown);
		}
		else
		{
			timeLeft.text = "" + Mathf.Floor(countDown);
		}
		timeLeft.text += ":";
		
		float milli =  (countDown - Mathf.Floor(countDown)) * 100;
		
		if(milli < 10)
		{
			timeLeft.text += "0" + Mathf.Floor(milli);
		}
		else
		{
			timeLeft.text += "" + Mathf.Floor(milli);
		}
		
		if(countDown < 5)
		{
			timeLeft.color = Color.red;
		}
	
	}
	public void useForce()
	{
		bruteAttempts--;
		if(bruteAttempts < 2)
		{
			attempts.color = Color.red;
		}
		attempts.text = bruteAttempts.ToString();

	}
	private void callFailed()
	{
		hackedObject.SendMessage("hackedFailed");
	}
	private void callHackedObject()
	{
		hackedObject.SendMessage("hackedSucceeded");
	}
}
