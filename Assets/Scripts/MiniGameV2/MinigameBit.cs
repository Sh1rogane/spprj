﻿using UnityEngine;
using System.Collections;

public class MinigameBit : MonoBehaviour 
{
	private ParticleSystem ps;
	private Renderer rend;
	private BoxCollider2D col;
	private AudioSource aS;
	private HackingGameV2 hg2;

	private Vector3 orgScale;
	private Vector3 beatScale;
	private float beatTimer = 1f / 2.1f;
	private float beatTime = 0f;
	// Use this for initialization
	void Start () 
	{
		aS = GetComponent<AudioSource>();
		ps = GetComponent<ParticleSystem>();
		rend = GetComponent<Renderer>();
		col = GetComponent<BoxCollider2D>();
		hg2 = GetComponentInParent<HackingGameV2>();

		orgScale = transform.localScale;
		beatScale = orgScale + new Vector3(0.03f, 0.03f, 0.03f);
		//beatScale = orgScale + new Vector3(0.12f, 0.12f, 0.12f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//print(Remap(Mathf.Sin(6.283185f * Time.time),-1,1,1,2));

		beatTime += Time.deltaTime;
		if(beatTime > beatTimer)
		{
			beatTime -= beatTimer;
			transform.localScale = beatScale;
		}

		transform.localScale = Vector3.Lerp(transform.localScale, orgScale, Time.deltaTime * 10);
	}
	public float Remap (float value, float from1, float to1, float from2, float to2) 
	{
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
	}
	private void kill()
	{
		col.enabled = false;
		rend.enabled = false;
		ps.Emit(30);
		aS.Play();
		hg2.removeBit(this);
		Destroy(gameObject, 1f);
	}
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.tag.Equals("MinigamePlayer"))
		{
			kill();
		}
		
	}
}
