﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MinigameTutorial2 : MonoBehaviour 
{
	
	private HackingGameV2 hg;
	
	private bool active;

	private int nextClicks = 0;
	
	// Use this for initialization
	void Start () 
	{
		hg = GetComponentInParent<HackingGameV2>();
		hg.active = false;
		
		active = true;
		
		Player.instance.hacking = true;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
	public void onNext()
	{
		nextClicks++;
		if(nextClicks == 1)
		{
			active = false;
			Invoke("endTutorial", 0.1f);
		}
	}
	public void endTutorial()
	{
		hg.active = true;
		Time.timeScale = 1;
		gameObject.SetActive(false);
	}
	// Update is called once per frame
	void Update () 
	{
		if(active)
		{
			hg.active = false;
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}
		
	}
}
