﻿using UnityEngine;
using System.Collections;

public class MinigameLaunchZone : MonoBehaviour {

	public int rotation = 0;
	public float speed = 0;
	public float pointDist;
	public float launchDelay;

	private float launchTimer;
	private int limit;
	private Vector3 point;
	private MinigamePlayer player;
	private bool launching;

	
	// Use this for initialization
	void Start ()
	{
		launching = false;
		player = transform.parent.GetComponentInChildren<MinigamePlayer>();
		limit = rotation;
		transform.FindChild("launchPoint").localPosition = new Vector3(pointDist,0,0);

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		point = transform.FindChild("launchPoint").position;
	
		if(Mathf.RoundToInt(transform.localRotation.eulerAngles.z) > limit)
		{
			rotation *= -1;
		}
		transform.Rotate(new Vector3(0,0,rotation * Time.deltaTime * speed));
		if(launching)
		{
			launchTimer += Time.deltaTime;
			if(launchTimer > launchDelay)
			{
				player.setToPosition(point);
				launching = false;
				launchTimer = 0;
			}
		}
	}
	public Vector3 getLaunchPoint()
	{
		return point;
	}
	private void LaunchMe()
	{
		launching = true;
	}
}
