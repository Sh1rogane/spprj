﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class MinigamePlayer : MonoBehaviour {

	private Camera gameCamera;
	private Vector3 toPosition;
	private Vector3 mousePosition;
	private TrailRenderer trail;
	private AudioSource aS;
	private HackingGameV2 hg2;

	private int speed;

	// Launcher related
	private bool indesposed;

	private LineRenderer lineHelper;


	// Use this for initialization
	void Start () 
	{
		gameCamera = transform.parent.GetComponentInChildren<Camera>();
		toPosition = transform.position;
		trail = GetComponent<TrailRenderer>();
		aS = GetComponent<AudioSource>();
		trail.sortingOrder = 99;
		hg2 = GetComponentInParent<HackingGameV2>();
		speed = 20;
		lineHelper = GetComponent<LineRenderer>();
		lineHelper.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(hg2.active){
			if(Input.GetButtonDown("Fire1"))
			{
				lineHelper.enabled = true;

			}
			if(lineHelper.enabled)
			{
				Vector3 mp = gameCamera.ScreenToWorldPoint(Input.mousePosition);
				mp.z = 0;
				lineHelper.SetPosition(0, transform.position);
				lineHelper.SetPosition(1, mp);
			}
			if(Input.GetButtonUp("Fire1") && hg2.bruteAttempts > 0 && canClick() && !indesposed)
			{
				lineHelper.enabled = false;

				hg2.useForce();
				
				mousePosition = gameCamera.ScreenToWorldPoint(Input.mousePosition);
				mousePosition.z = 0;

				fixDest (mousePosition);

				if(!mousePosition.Equals(toPosition))
				{
					gameCamera.GetComponent<CameraShake>().shake = 1;
				}
			

				aS.Play();
			}
			float dist = Vector2.Distance(toPosition, transform.position);
			transform.position = Vector3.Lerp(transform.position, toPosition, Time.deltaTime * (speed/dist));
		}


	}
	private void fixDest(Vector3 v)
	{
		Vector3 dirr = v - transform.position;
		LayerMask lm = 1 << 2;
		lm |= 1 << 17;
		lm |= 1 << 18;
		lm = ~lm;
		float dist = Vector2.Distance(v, transform.position);
		RaycastHit2D hit = Physics2D.Raycast(transform.position, dirr, dist, lm);
		if(hit)
		{
			Debug.DrawLine(transform.position, hit.point, Color.blue, 1f);
			Vector3 h = hit.point;
			
			h -= dirr.normalized * GetComponent<CircleCollider2D>().radius / 2;
			toPosition = h;
		}
		else
		{
			toPosition = v;

		}
	}
	public void setToPosition(Vector3 p)
	{
		fixDest(toPosition = p);
	}
	public bool isMoving()
	{
		float dist = Vector2.Distance(toPosition, transform.position);
		if(dist < 0.01f)
		{
			return false;
		}
		return true;
	}
	private bool canClick()
	{

		Vector2 pos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
		
		Collider2D[] col = Physics2D.OverlapPointAll(pos);
		
		if(col.Length > 0)
		{
			foreach(Collider2D c in col)
			{
				if(c.tag.Equals("MinigameTimezone"))
				{
					gameCamera.GetComponent<CameraShake>().shake = 1;
					return false;
				}
			}
		}

		return true;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.tag == "MinigameTimezone")
		{
			Time.timeScale = 0.1f;
			gameCamera.GetComponent<MotionBlur>().blurAmount = 0.92f;
		}

		if(coll.tag == "MinigameLaunchZone")
		{
			indesposed = true;
			toPosition = coll.transform.position;
			coll.SendMessage("LaunchMe");
		}
	}
	void OnTriggerStay2D(Collider2D coll)
	{
		if(coll.tag == "MinigameTimezone")
		{
			Time.timeScale = 0.1f;
		}
		if(coll.tag == "MinigameLaunchZone")
		{
			indesposed = true;
		}
	}
	void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.tag == "MinigameTimezone")
		{
			Time.timeScale = 1f;
			gameCamera.GetComponent<MotionBlur>().blurAmount = 0f;
		}

		if(coll.tag == "MinigameLaunchZone")
		{
			indesposed = false;
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.DrawSphere(toPosition, 0.3f);
	}
}
