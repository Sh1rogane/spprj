﻿using UnityEngine;
using System.Collections;

public class EndOfLevel : MonoBehaviour {

	private Player player;
	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindWithTag("Player").GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.tag.Equals("Player"))
		{
			player.kill ();
			CheckpointSystem.checkpointIndex = 0;

		}
	
	}
}
