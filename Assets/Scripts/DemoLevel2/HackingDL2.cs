﻿using UnityEngine;
using System.Collections;

public class HackingDL2 : MonoBehaviour 
{
	private Object hgPrefab;
	private GameObject hg;
	private HackingGameV2 hgScript;

	public Door closeDoor;
	public Door openDoor;

	private bool canHack = true;
	private Interactable interactable;
	// Use this for initialization
	void Start () 
	{
		hgPrefab = Resources.Load("HackingGame/HackingGame Level 1");
		interactable = GetComponent<Interactable>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	
	}
	public void interactUse()
	{
		if(canHack)
		{
			createGame();
		}

	}
	private void createGame()
	{
		hg = Instantiate (hgPrefab) as GameObject;
		hgScript = hg.GetComponent<HackingGameV2>();
		hgScript.hackedObject = this.gameObject;
		Invoke("start", 0.1f);
	}
	public void start()
	{
		hgScript.start();
	}
	public void hackedSucceeded()
	{
		interactable.enabled = false;
		//Ful fix
		Destroy(interactable);
		canHack = false;
		closeDoor.closeDoor();
		openDoor.openDoor();
	}

	public void hackedFailed()
	{

	}
}
