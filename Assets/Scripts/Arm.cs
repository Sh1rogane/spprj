﻿using UnityEngine;
using System.Collections;

public class Arm : MonoBehaviour 
{
	private GameObject gadgetCamera;
	private GameObject motionSensor;
	private GameObject noiseMaker;

	private Player player;

	private Animator ani;
	// Use this for initialization
	void Start () 
	{
		gadgetCamera = transform.Find("Scoulder_Joint/Elbow_Joint/Wrist_Joint/Camera").gameObject;
		motionSensor = transform.Find("Scoulder_Joint/Elbow_Joint/Wrist_Joint/Motion Sensor").gameObject;
		noiseMaker = transform.Find("Scoulder_Joint/Elbow_Joint/Wrist_Joint/Noise Maker").gameObject;

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		gadgetCamera.SetActive(false);
		noiseMaker.SetActive(false);

		ani = GetComponent<Animator>();
	}
	public bool isInChangeAnimation()
	{
		if(ani.GetCurrentAnimatorStateInfo(0).IsName("Arm Change"))
		{
			return true;
		}
		return false;
	}
	public bool isInThrowAnimation()
	{
		return ani.GetCurrentAnimatorStateInfo(0).IsName("Arm Throw");
	}
	// Update is called once per frame
	void Update () 
	{
		if(player.motionSensor == 0)
		{
			motionSensor.SetActive(false);
		}
		if(player.noiseMaker == 0)
		{
			noiseMaker.SetActive(false);
		}
		if(player.gadgetCamera == 0)
		{
			gadgetCamera.SetActive(false);
		}
	}
	public void throwGadget()
	{
		ani.SetTrigger("throw");
		ani.SetBool("raise", false);
	}
	public void raise()
	{
		ani.SetBool("raise", true);
	}
	public void use()
	{
		ani.SetTrigger("use");
	}
	public void setSpeed(float speed)
	{
		ani.SetFloat("speed", speed);
	}
	public void change()
	{
		ani.SetTrigger("change");
	}
	public void onChange()
	{
		if(player.getGadgetIndex() == Player.MOTIONSENSOR)
		{
			motionSensor.SetActive(true);
			gadgetCamera.SetActive(false);
			noiseMaker.SetActive(false);

			if(player.motionSensor == 0)
			{
				motionSensor.SetActive(false);
			}
		}
		else if(player.getGadgetIndex() == Player.NOISEMAKER)
		{
			motionSensor.SetActive(false);
			gadgetCamera.SetActive(false);
			noiseMaker.SetActive(true);

			if(player.noiseMaker == 0)
			{
				noiseMaker.SetActive(false);
			}
		}
		else if(player.getGadgetIndex() == Player.GADGETCAMERA)
		{
			motionSensor.SetActive(false);
			gadgetCamera.SetActive(true);
			noiseMaker.SetActive(false);

			if(player.gadgetCamera == 0)
			{
				gadgetCamera.SetActive(false);
			}
		}
	}
}
