﻿using UnityEngine;
using System.Collections;

public class Minimap : MonoBehaviour 
{

	public Transform target;

	public float offsetY = 3;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.position = new Vector3(target.position.x, target.position.y + offsetY, target.position.z);
		Vector3 rot = target.rotation.eulerAngles;
		rot.x = 90;
		this.transform.rotation = Quaternion.Euler(rot);
	}
}
