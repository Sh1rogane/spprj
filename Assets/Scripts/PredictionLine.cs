﻿using UnityEngine;
using System.Collections;

public class PredictionLine : MonoBehaviour 
{

	private LineRenderer lineRenderer;
	private GameObject bullseye;
	// Use this for initialization
	void Start ()
	{
		lineRenderer = GetComponent<LineRenderer>();
		bullseye = transform.Find("Bullseye").gameObject;
		showPrediction(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	public void showPrediction(bool b)
	{
		lineRenderer.enabled = b;
		bullseye.GetComponent<Renderer>().enabled = b;
	}
	public void updateTrajectory(Vector3 initialPosition, Vector3 initialVelocity)
	{
		int numSteps = 20; // for example
		float timeDelta = 1.0f / initialVelocity.magnitude; // for example

		lineRenderer.SetVertexCount(numSteps);
		
		Vector3 position = initialPosition;
		Vector3 velocity = initialVelocity;
		for (int i = 0; i < numSteps; ++i)
		{
			lineRenderer.SetPosition(i, position);

			Vector3 maxDist = velocity * timeDelta + 0.5f *  Physics.gravity * timeDelta * timeDelta;
			//relativePos.magnitude;

			LayerMask lm = 1 << 10;
			lm |= 1 << 2; 
			lm |= 1 << 8; 
			lm |= 1 << 9; 
			lm |= 1 << 12; 
			lm |= 1 << 13;

			lm = ~lm;

			RaycastHit hit;
			if(Physics.Raycast(position, velocity.normalized, out hit, maxDist.magnitude, lm))
			{
				lineRenderer.SetVertexCount(i + 2);
				lineRenderer.SetPosition(i + 1, hit.point);
			

				bullseye.transform.position = hit.point + (hit.normal * 0.01f);
				bullseye.transform.rotation = Quaternion.LookRotation(hit.normal);

				break;
			}

			position += velocity * timeDelta + 0.5f *  Physics.gravity * timeDelta * timeDelta;
			velocity +=  Physics.gravity * timeDelta;
		}
	}
}
