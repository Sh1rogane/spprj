﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HelpDisplay))]

public class HelpDisplayEditor : Editor 
{
	private Vector2 scroll;
	public override void OnInspectorGUI()
	{
		HelpDisplay hd = target as HelpDisplay;

		EditorGUILayout.LabelField("Text");
		EditorStyles.textField.wordWrap = true;
		hd.text = EditorGUILayout.TextArea(hd.text);

		EditorUtility.SetDirty(hd);
	}
}
#endif