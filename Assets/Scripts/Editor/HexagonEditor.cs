﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

using UnityEditor;

[CustomEditor(typeof(Hexagon))]
public class HexagonEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		//DrawDefaultInspector();
		
		Hexagon hg = target as Hexagon;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.FloatField("Side", Mathf.RoundToInt(hg.transform.eulerAngles.z) / 60);
		if(GUILayout.Button("Rotate 1 Left"))
		{
			hg.transform.Rotate(0,0,60);
			hg.transform.rotation = Quaternion.Euler(0,0, Mathf.Round(hg.transform.eulerAngles.z));
		}
		if(GUILayout.Button("Rotate 1 Right"))
		{
			hg.transform.Rotate(0,0,-60);
			hg.transform.rotation = Quaternion.Euler(0,0, Mathf.Round(hg.transform.eulerAngles.z));
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Set as correct side"))
		{
			hg.correctSide = Mathf.RoundToInt(hg.transform.eulerAngles.z) / 60;
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.FloatField("Correct Side", hg.correctSide);

		hg.hasAltCorrect = EditorGUILayout.Toggle("Has alternative correct", hg.hasAltCorrect);
		if(hg.hasAltCorrect)
		{
			if(GUILayout.Button("Set as alternative correct side"))
			{
				hg.altCorrectSide = Mathf.RoundToInt(hg.transform.eulerAngles.z) / 60;
			}
			EditorGUILayout.FloatField("Alternative Correct Side", hg.altCorrectSide);
		}
		EditorUtility.SetDirty(hg);

	}
}
#endif