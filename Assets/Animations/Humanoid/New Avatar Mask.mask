%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 00000000000000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Main_Character2:Mesh
    m_Weight: 1
  - m_Path: Root
    m_Weight: 1
  - m_Path: Root/L_Hip
    m_Weight: 1
  - m_Path: Root/L_Hip/L_Knee
    m_Weight: 1
  - m_Path: Root/L_Hip/L_Knee/L_Ankle
    m_Weight: 1
  - m_Path: Root/L_Hip/L_Knee/L_Ankle/L_Foot
    m_Weight: 1
  - m_Path: Root/R_Hip
    m_Weight: 1
  - m_Path: Root/R_Hip/R_Knee
    m_Weight: 1
  - m_Path: Root/R_Hip/R_Knee/R_Ankle
    m_Weight: 1
  - m_Path: Root/R_Hip/R_Knee/R_Ankle/R_Foot
    m_Weight: 1
  - m_Path: Root/Spine_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/Headjoint
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/Headjoint/Headtip
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Indexfinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Indexfinger_01/L_indexfinger_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Middlefinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Middlefinger_01/L_Middlefinger_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Pinky_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Pinky_01/L_Pinky_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Ringfinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Ringfinger_01/L_Ringfinder_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Thumb_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/L_Shoulder/L_Upperarm/L_Elbow/L_Hand/L_Thumb_01/L_Thumb_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Indexfinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Indexfinger_01/R_indexfinger_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Middlefinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Middlefinger_01/R_Middlefinger_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Pinky_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Pinky_01/R_Pinky_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Ringfinger_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Ringfinger_01/R_Ringfinder_02
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Thumb_01
    m_Weight: 1
  - m_Path: Root/Spine_01/Neck/R_Shoulder/R_Upperarm/R_Elbow/R_Hand/R_Thumb_01/R_Thumb_02
    m_Weight: 1
